"use strict";

var ChineseZodiac = function ChineseZodiac() {
    this.data_mapping = {
        "0": {
            "en": "Monkey",
            "cn": "猴"
        },
        "1": {
            "en": "Rooster",
            "cn": "鸡"
        },
        "2": {
            "en": "Dog",
            "cn": "狗"
        },
        "3": {
            "en": "Pig",
            "cn": "猪"
        },
        "4": {
            "en": "Rat",
            "cn": "鼠"
        },
        "5": {
            "en": "Ox",
            "cn": "牛"
        },
        "6": {
            "en": "Tiger",
            "cn": "虎"
        },
        "7": {
            "en": "Rabbit",
            "cn": "兔"
        },
        "8": {
            "en": "Dragon",
            "cn": "龙"
        },
        "9": {
            "en": "Snake",
            "cn": "蛇"
        },
        "10": {
            "en": "Horse",
            "cn": "马"
        },
        "11": {
            "en": "Goat",
            "cn": "羊"
        }
    };
};

ChineseZodiac.prototype.getValueString = function (key, lang) {
    return this.data_mapping[key] == null ? null : this.data_mapping[key][lang];
};

module.exports = ChineseZodiac;