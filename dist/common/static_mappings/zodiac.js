'use strict';

var Zodiac = function Zodiac() {
    this.data_mapping = {
        '1': {
            'en': 'aries',
            'cn': '白羊座'
        },
        '2': {
            'en': 'taurus',
            'cn': '金牛座'
        },
        '3': {
            'en': 'gemini',
            'cn': '双子座'
        },
        '4': {
            'en': 'cancer',
            'cn': '巨蟹座'
        },
        '5': {
            'en': 'leo',
            'cn': '狮子座'
        },
        '6': {
            'en': 'virgo',
            'cn': '处女座'
        },
        '7': {
            'en': 'libra',
            'cn': '天秤座'
        },
        '8': {
            'en': 'scorpio',
            'cn': '天蝎座'
        },
        '9': {
            'en': 'sagittarius',
            'cn': '射手座'
        },
        '10': {
            'en': 'capricorn',
            'cn': '摩羯座'
        },
        '11': {
            'en': 'aquarius',
            'cn': '水瓶座'
        },
        '12': {
            'en': 'pisces',
            'cn': '双鱼座'
        }
    };
};

Zodiac.prototype.getValueString = function (key, lang) {
    return this.data_mapping[key] == null ? null : this.data_mapping[key][lang];
};

module.exports = Zodiac;