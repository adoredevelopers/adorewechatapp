"use strict";

var EducationLevel = function EducationLevel() {
    this.data_mapping = {
        "-1": {
            "en": "Not Specified",
            "cn": "未填"
        },
        "1": {
            "en": "Self Taught",
            "cn": "自学成才"
        },
        "2": {
            "en": "Secondary",
            "cn": "高中"
        },
        "3": {
            "en": "Post Secondary",
            "cn": "大专"
        },
        "4": {
            "en": "Bachelor",
            "cn": "本科"
        },
        "5": {
            "en": "Master",
            "cn": "研究生"
        },
        "6": {
            "en": "PhD.",
            "cn": "博士生"
        },
        "7": {
            "en": "Post Doc",
            "cn": "博士后"
        }
    };
};

EducationLevel.prototype.getValueString = function (key, lang) {
    return this.data_mapping[key] == null ? null : this.data_mapping[key][lang];
};

module.exports = EducationLevel;