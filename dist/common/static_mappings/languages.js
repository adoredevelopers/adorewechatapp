"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _wepy = require('./../../npm/wepy/lib/wepy.js');

var _wepy2 = _interopRequireDefault(_wepy);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var Languages = function (_wepy$component) {
    _inherits(Languages, _wepy$component);

    function Languages() {
        var _ref;

        var _temp, _this, _ret;

        _classCallCheck(this, Languages);

        for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
            args[_key] = arguments[_key];
        }

        return _ret = (_temp = (_this = _possibleConstructorReturn(this, (_ref = Languages.__proto__ || Object.getPrototypeOf(Languages)).call.apply(_ref, [this].concat(args))), _this), _this.props = {
            data_mapping: {
                "1": {
                    "en": "English",
                    "cn": "英文"
                },
                "2": {
                    "en": "Mandarin",
                    "cn": "国语"
                },
                "3": {
                    "en": "Cantonese",
                    "cn": "粤语（繁体字）"
                },
                "4": {
                    "en": "French",
                    "cn": "法语"
                },
                "5": {
                    "en": "Afrikaans",
                    "cn": "南非洲的荷兰语"
                },
                "6": {
                    "en": "Albanian",
                    "cn": "阿尔巴尼亚语"
                },
                "7": {
                    "en": "Arabic",
                    "cn": "阿拉伯语"
                },
                "8": {
                    "en": "Armenian",
                    "cn": "亚美尼亚语"
                },
                "9": {
                    "en": "Basque",
                    "cn": "巴斯克语"
                },
                "10": {
                    "en": "Bengali",
                    "cn": "孟加拉语"
                },
                "11": {
                    "en": "Bulgarian",
                    "cn": "保加利亚语"
                },
                "12": {
                    "en": "Catalan",
                    "cn": "加泰罗尼亚语"
                },
                "13": {
                    "en": "Cambodian",
                    "cn": "柬埔寨语"
                },
                "14": {
                    "en": "Croatian",
                    "cn": "克罗地亚人语"
                },
                "15": {
                    "en": "Czech",
                    "cn": "捷克语"
                },
                "16": {
                    "en": "Danish",
                    "cn": "丹麦语"
                },
                "17": {
                    "en": "Dutch",
                    "cn": "荷兰语"
                },
                "18": {
                    "en": "Estonian",
                    "cn": "爱沙尼亚语"
                },
                "19": {
                    "en": "Fiji",
                    "cn": "斐济语"
                },
                "20": {
                    "en": "Finnish",
                    "cn": "芬兰语"
                },
                "21": {
                    "en": "Georgian",
                    "cn": "乔治亚语"
                },
                "22": {
                    "en": "German",
                    "cn": "德语"
                },
                "23": {
                    "en": "Greek",
                    "cn": "希腊语"
                },
                "24": {
                    "en": "Gujarati",
                    "cn": "古吉拉特语"
                },
                "25": {
                    "en": "Hebrew",
                    "cn": "希伯来语"
                },
                "26": {
                    "en": "Hindi",
                    "cn": "北印度语"
                },
                "27": {
                    "en": "Hungarian",
                    "cn": "匈牙利语"
                },
                "28": {
                    "en": "Icelandic",
                    "cn": "冰岛语"
                },
                "29": {
                    "en": "Indonesian",
                    "cn": "印尼语群"
                },
                "30": {
                    "en": "Irish",
                    "cn": "爱尔兰语"
                },
                "31": {
                    "en": "Italian",
                    "cn": "意大利语"
                },
                "32": {
                    "en": "Japanese",
                    "cn": "日语"
                },
                "33": {
                    "en": "Javanese",
                    "cn": "爪哇语"
                },
                "34": {
                    "en": "Korean",
                    "cn": "韩语"
                },
                "35": {
                    "en": "Latin",
                    "cn": "拉丁语"
                },
                "36": {
                    "en": "Latvian",
                    "cn": "拉脱维亚语"
                },
                "37": {
                    "en": "Lithuanian",
                    "cn": "立陶宛语"
                },
                "38": {
                    "en": "Macedonian",
                    "cn": "马其顿语"
                },
                "39": {
                    "en": "Malay",
                    "cn": "马来语"
                },
                "40": {
                    "en": "Malayalam",
                    "cn": "印度西南部语"
                },
                "41": {
                    "en": "Maltese",
                    "cn": "马尔他语"
                },
                "42": {
                    "en": "Maori",
                    "cn": "毛利语"
                },
                "43": {
                    "en": "Marathi",
                    "cn": "马拉地语"
                },
                "44": {
                    "en": "Mongolian",
                    "cn": "蒙古语"
                },
                "45": {
                    "en": "Nepali",
                    "cn": "尼泊尔语"
                },
                "46": {
                    "en": "Norwegian",
                    "cn": "挪威语"
                },
                "47": {
                    "en": "Persian",
                    "cn": "波斯语"
                },
                "48": {
                    "en": "Polish",
                    "cn": "波兰语"
                },
                "49": {
                    "en": "Portuguese",
                    "cn": "葡萄牙语"
                },
                "50": {
                    "en": "Punjabi",
                    "cn": "旁遮普语"
                },
                "51": {
                    "en": "Quechua",
                    "cn": "盖丘亚语"
                },
                "52": {
                    "en": "Romanian",
                    "cn": "罗马尼亚语"
                },
                "53": {
                    "en": "Russian",
                    "cn": "俄语"
                },
                "54": {
                    "en": "Samoan",
                    "cn": "萨摩亚语"
                },
                "55": {
                    "en": "Serbian",
                    "cn": "塞尔维亚语"
                },
                "56": {
                    "en": "Slovak",
                    "cn": "斯洛伐克人语"
                },
                "57": {
                    "en": "Slovenian",
                    "cn": "斯洛文尼亚语"
                },
                "58": {
                    "en": "Spanish",
                    "cn": "西班牙语"
                },
                "59": {
                    "en": "Swahili",
                    "cn": "斯瓦希里语"
                },
                "60": {
                    "en": "Swedish",
                    "cn": "瑞典语"
                },
                "61": {
                    "en": "Tamil",
                    "cn": "坦米尔语"
                },
                "62": {
                    "en": "Tatar",
                    "cn": "鞑靼语"
                },
                "63": {
                    "en": "Telugu",
                    "cn": "泰卢固语"
                },
                "64": {
                    "en": "Thai",
                    "cn": "泰国语"
                },
                "65": {
                    "en": "Tibetan",
                    "cn": "藏语"
                },
                "66": {
                    "en": "Tonga",
                    "cn": "汤加语"
                },
                "67": {
                    "en": "Turkish",
                    "cn": "土耳其语"
                },
                "68": {
                    "en": "Ukrainian",
                    "cn": "乌克兰语"
                },
                "69": {
                    "en": "Urdu",
                    "cn": "乌尔都语"
                },
                "70": {
                    "en": "Uzbek",
                    "cn": "乌兹别克语"
                },
                "71": {
                    "en": "Vietnamese",
                    "cn": "土耳其语"
                },
                "72": {
                    "en": "Welsh",
                    "cn": "威尔士语"
                },
                "73": {
                    "en": "Xhosa",
                    "cn": "班图语"
                },
                "999": {
                    "en": "Other",
                    "cn": "其它"
                }
            }
        }, _this.data = {}, _this.events = {}, _this.methods = {
            getValueString: function getValueString(key, lang) {
                return this.props.data_mapping[key][lang];
            }
        }, _temp), _possibleConstructorReturn(_this, _ret);
    }

    _createClass(Languages, [{
        key: "getValueString",
        value: function getValueString(key, lang) {
            return this.props.data_mapping[key][lang];
        }
    }]);

    return Languages;
}(_wepy2.default.component);

exports.default = Languages;