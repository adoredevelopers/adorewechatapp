"use strict";

var BodyShape = function BodyShape() {
    this.data_mapping = {
        "-1": {
            "en": "Not Specified",
            "cn": "未填"
        },
        "1": {
            "en": "Cute 'n' Tiny",
            "cn": "小巧"
        },
        "2": {
            "en": "Slim",
            "cn": "纤细"
        },
        "3": {
            "en": "Slender",
            "cn": "苗条"
        },
        "4": {
            "en": "Fit",
            "cn": "匀称"
        },
        "5": {
            "en": "Athletic",
            "cn": "运动型"
        },
        "6": {
            "en": "Typical",
            "cn": "正常"
        },
        "7": {
            "en": "Built",
            "cn": "结实"
        },
        "8": {
            "en": "Muscular",
            "cn": "健美型"
        },
        "9": {
            "en": "Brawny",
            "cn": "肌肉型"
        },
        "10": {
            "en": "Curvy",
            "cn": "凹凸有致"
        },
        "11": {
            "en": "Chubby",
            "cn": "有点小肉"
        },
        "12": {
            "en": "Rubenesque",
            "cn": "丰满"
        },
        "13": {
            "en": "Very Huggy",
            "cn": "大抱枕"
        }
    };
};

BodyShape.prototype.getValueString = function (key, lang) {
    return this.data_mapping[key] == null ? null : this.data_mapping[key][lang];
};

module.exports = BodyShape;