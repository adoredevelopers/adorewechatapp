"use strict";

var Occupation = function Occupation() {
    this.data_mapping = {
        "-1": {
            "en": "Not Specified",
            "cn": "未填"
        },
        "1": {
            "en": "Accountants",
            "cn": "会计"
        },
        "2": {
            "en": "Actuaries",
            "cn": "精算师"
        },
        "3": {
            "en": "Administrative",
            "cn": "行政"
        },
        "4": {
            "en": "Advertising",
            "cn": "广告"
        },
        "5": {
            "en": "Aerospace Engineers",
            "cn": "航空航天工程师"
        },
        "6": {
            "en": "Agricultural Engineers",
            "cn": "农业工程"
        },
        "7": {
            "en": "Agricultural Representatives, Consultants and Specialists",
            "cn": "农业专家"
        },
        "8": {
            "en": "Air Traffic Controllers",
            "cn": "空管"
        },
        "9": {
            "en": "Aircraft Mechanics",
            "cn": "航空技师"
        },
        "10": {
            "en": "Airworthiness Inspectors",
            "cn": "航空器适航监察员"
        },
        "11": {
            "en": "Animal Health Technologists",
            "cn": "兽医"
        },
        "12": {
            "en": "Animation Painters",
            "cn": "动漫家"
        },
        "13": {
            "en": "Anthropologists",
            "cn": "人类学家"
        },
        "14": {
            "en": "Appraisers",
            "cn": "估价师"
        },
        "15": {
            "en": "Archaeologists",
            "cn": "考古学家"
        },
        "16": {
            "en": "Architects",
            "cn": "建筑师"
        },
        "17": {
            "en": "Archivists",
            "cn": "档案专业人员"
        },
        "18": {
            "en": "Astronomers",
            "cn": "天文学家"
        },
        "19": {
            "en": "Audiologists",
            "cn": "听力学家"
        },
        "20": {
            "en": "Audio and Video Recording Technicians",
            "cn": "音频与视频录入技师"
        },
        "21": {
            "en": "Avionics Inspectors, Mechanics and Technicians",
            "cn": "电子设备专家"
        },
        "22": {
            "en": "Bakers",
            "cn": "面包师"
        },
        "23": {
            "en": "Barbers",
            "cn": "理发师"
        },
        "24": {
            "en": "Biological Technicians",
            "cn": "生物学家"
        },
        "25": {
            "en": "Biomedical Engineers",
            "cn": "生物工程师"
        },
        "26": {
            "en": "Blacksmiths",
            "cn": "铁匠"
        },
        "27": {
            "en": "Boilermakers",
            "cn": "锅炉制造工"
        },
        "28": {
            "en": "Bookkeepers",
            "cn": "簿记员"
        },
        "29": {
            "en": "Broadcast Technicians",
            "cn": "播音技师"
        },
        "30": {
            "en": "Cabinetmakers",
            "cn": "家具木工"
        },
        "31": {
            "en": "Cable Television Service Technicians",
            "cn": "有线电视服务与维护技术员"
        },
        "32": {
            "en": "Carvers",
            "cn": "雕刻匠"
        },
        "33": {
            "en": "Chefs ",
            "cn": "大厨"
        },
        "34": {
            "en": "Chemical Engineers",
            "cn": "化学工程师"
        },
        "35": {
            "en": "Chemists",
            "cn": "配药师"
        },
        "36": {
            "en": "Civil Engineering",
            "cn": "土木工程"
        },
        "37": {
            "en": "Computer Hardware Engineers",
            "cn": "计算机硬件工程师"
        },
        "38": {
            "en": "Conservators",
            "cn": "保护区管理员"
        },
        "39": {
            "en": "Cooks",
            "cn": "炊事员"
        },
        "40": {
            "en": "Copywriters",
            "cn": "文案人员"
        },
        "41": {
            "en": "Curatorial Assistants",
            "cn": "馆长助理"
        },
        "42": {
            "en": "Dental Technicians",
            "cn": "牙科技术师"
        },
        "43": {
            "en": "Denturists",
            "cn": "牙托技师"
        },
        "44": {
            "en": "Directors",
            "cn": "董事"
        },
        "45": {
            "en": "Driver's Licence Examiners",
            "cn": "驾照考官"
        },
        "46": {
            "en": "Driving Instructors",
            "cn": "驾照教官"
        },
        "47": {
            "en": "Economists",
            "cn": "经济学家"
        },
        "48": {
            "en": "Editors",
            "cn": "编辑"
        },
        "49": {
            "en": "Electrical Mechanics",
            "cn": "电子技师"
        },
        "51": {
            "en": "Executive Chefs",
            "cn": "特级厨师"
        },
        "52": {
            "en": "Fashion Designers",
            "cn": "时装设计师"
        },
        "53": {
            "en": "Financial and Investment Analysts",
            "cn": "金融投资分析师"
        },
        "54": {
            "en": "Financial Planners",
            "cn": "财经管理"
        },
        "55": {
            "en": "Firefighters",
            "cn": "消防员"
        },
        "56": {
            "en": "Historians",
            "cn": "历史学家"
        },
        "57": {
            "en": "Industrial Engineering",
            "cn": "制造工程师"
        },
        "58": {
            "en": "Insurance Clerks",
            "cn": "保险管理员"
        },
        "59": {
            "en": "Interpreters",
            "cn": "翻译"
        },
        "60": {
            "en": "Jewellers and Related Workers",
            "cn": "珠宝商"
        },
        "61": {
            "en": "Journalists",
            "cn": "记者"
        },
        "62": {
            "en": "Landscape Architects",
            "cn": "景观设计师"
        },
        "63": {
            "en": "Legal Assistants and Paralegals",
            "cn": "法律助理"
        },
        "64": {
            "en": "Librarians",
            "cn": "图书管理员"
        },
        "65": {
            "en": "Locksmiths",
            "cn": "开锁匠"
        },
        "66": {
            "en": "Machinists",
            "cn": "机械师"
        },
        "67": {
            "en": "Mechanical Engineers",
            "cn": "机械工程师"
        },
        "68": {
            "en": "Medical Technologists",
            "cn": "医学技师"
        },
        "69": {
            "en": "Meteorologists",
            "cn": "气象学家"
        },
        "70": {
            "en": "Orthopaedic Technologists",
            "cn": "整形外科技师"
        },
        "71": {
            "en": "Patent Agents",
            "cn": "专利师"
        },
        "73": {
            "en": "Photographers",
            "cn": "照相师"
        },
        "74": {
            "en": "Physicists",
            "cn": "物理学家"
        },
        "75": {
            "en": "Potters",
            "cn": "陶工"
        },
        "76": {
            "en": "Psychologists",
            "cn": "心理学家"
        },
        "77": {
            "en": "Psychometricians",
            "cn": "精神治疗医师"
        },
        "78": {
            "en": "Railway Locomotive Engineers",
            "cn": "火车技师"
        },
        "79": {
            "en": "Teacher",
            "cn": "老师"
        },
        "80": {
            "en": "Seamstresses",
            "cn": "裁缝"
        },
        "81": {
            "en": "Sociologists",
            "cn": "社会学家"
        },
        "82": {
            "en": "Software Engineers",
            "cn": "软件工程师"
        },
        "83": {
            "en": "Taxidermists",
            "cn": "剥制师"
        },
        "85": {
            "en": "Upholsterers",
            "cn": "室内装潢商"
        },
        "86": {
            "en": "Doctor",
            "cn": "医生"
        },
        "87": {
            "en": "Lawer",
            "cn": "律师"
        },
        "88": {
            "en": "Driver",
            "cn": "司机"
        },
        "999": {
            "en": "Other",
            "cn": "其它"
        }
    };
};

Occupation.prototype.getValueString = function (key, lang) {
    return this.data_mapping[key] == null ? null : this.data_mapping[key][lang];
};

module.exports = Occupation;