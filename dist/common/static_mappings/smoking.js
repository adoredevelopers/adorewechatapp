"use strict";

var Smoking = function Smoking() {
    this.data_mapping = {
        "-1": {
            "en": "Not Specified",
            "cn": "未填"
        },
        "1": {
            "en": "No",
            "cn": "不吸烟"
        },
        "2": {
            "en": "Seldomly",
            "cn": "很少"
        },
        "3": {
            "en": "Sometimes",
            "cn": "偶尔"
        },
        "4": {
            "en": "Often",
            "cn": "经常"
        },
        "5": {
            "en": "Frequent",
            "cn": "频繁"
        }
    };
};

Smoking.prototype.getValueString = function (key, lang) {
    return this.data_mapping[key] == null ? null : this.data_mapping[key][lang];
};

module.exports = Smoking;