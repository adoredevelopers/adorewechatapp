'use strict';

var Activity = function Activity() {
    this.data_mapping = {
        '1': {
            'en': 'tea',
            'cn': '喝茶'
        },
        '2': {
            'en': 'coffee',
            'cn': '咖啡'
        },
        '3': {
            'en': 'board games',
            'cn': '桌游'
        },
        '4': {
            'en': 'food',
            'cn': '美食'
        },
        '5': {
            'en': 'drinks',
            'cn': '喝酒'
        },
        '6': {
            'en': 'froyo',
            'cn': '霜冻优格/冰激凌'
        },
        '7': {
            'en': 'bowling',
            'cn': '保龄球'
        },
        '8': {
            'en': 'dart',
            'cn': '飞镖'
        },
        '9': {
            'en': 'go kart',
            'cn': '卡丁车'
        },
        '10': {
            'en': 'mini golf/putting',
            'cn': '高尔夫'
        },
        '11': {
            'en': 'karaoke',
            'cn': '唱K'
        },
        '12': {
            'en': 'other',
            'cn': '其它'
        }
    };
};

Activity.prototype.getValueString = function (key, lang) {
    return this.data_mapping[key] == null ? null : this.data_mapping[key][lang];
};

module.exports = Activity;