'use strict';

var MartialStatus = function MartialStatus() {
    this.data_mapping = {
        '-1': {
            'en': 'undisclosed',
            'cn': '未说明'
        },
        '1': {
            'en': 'never married',
            'cn': '未婚'
        },
        '2': {
            'en': 'divorced',
            'cn': '离异'
        },
        '3': {
            'en': 'widowed',
            'cn': '丧偶'
        },
        '4': {
            'en': 'separated',
            'cn': '分居/待离'
        }
    };
};

MartialStatus.prototype.getValueString = function (key, lang) {
    return this.data_mapping[key] == null ? null : this.data_mapping[key][lang];
};

module.exports = MartialStatus;