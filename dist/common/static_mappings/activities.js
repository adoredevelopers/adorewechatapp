'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _wepy = require('./../../npm/wepy/lib/wepy.js');

var _wepy2 = _interopRequireDefault(_wepy);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var Activities = function (_wepy$component) {
    _inherits(Activities, _wepy$component);

    function Activities() {
        var _ref;

        var _temp, _this, _ret;

        _classCallCheck(this, Activities);

        for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
            args[_key] = arguments[_key];
        }

        return _ret = (_temp = (_this = _possibleConstructorReturn(this, (_ref = Activities.__proto__ || Object.getPrototypeOf(Activities)).call.apply(_ref, [this].concat(args))), _this), _this.props = {
            data_mapping: {
                '1': {
                    'en': 'tea',
                    'cn': '喝茶'
                },
                '2': {
                    'en': 'coffee',
                    'cn': '咖啡'
                },
                '3': {
                    'en': 'board games',
                    'cn': '桌游'
                },
                '4': {
                    'en': 'food',
                    'cn': '美食'
                },
                '5': {
                    'en': 'drinks',
                    'cn': '喝酒'
                },
                '6': {
                    'en': 'froyo',
                    'cn': '霜冻优格/冰激凌'
                },
                '7': {
                    'en': 'bowling',
                    'cn': '保龄球'
                },
                '8': {
                    'en': 'dart',
                    'cn': '飞镖'
                },
                '9': {
                    'en': 'go kart',
                    'cn': '卡丁车'
                },
                '10': {
                    'en': 'mini golf/putting',
                    'cn': '高尔夫'
                },
                '11': {
                    'en': 'karaoke',
                    'cn': '唱K'
                },
                '12': {
                    'en': 'other',
                    'cn': '其它'
                }
            }
        }, _this.data = {}, _this.events = {}, _this.methods = {
            getValueString: function getValueString(key, lang) {
                return this.props.data_mapping[key][lang];
            }
        }, _temp), _possibleConstructorReturn(_this, _ret);
    }

    _createClass(Activities, [{
        key: 'getValueString',
        value: function getValueString(key, lang) {
            return this.props.data_mapping[key][lang];
        }
    }]);

    return Activities;
}(_wepy2.default.component);

exports.default = Activities;