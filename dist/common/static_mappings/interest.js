"use strict";

var Interest = function Interest() {
    this.data_mapping = {
        "1": {
            "en": "Aircraft Spotting",
            "cn": "航观"
        },
        "2": {
            "en": "Acting",
            "cn": "演戏"
        },
        "3": {
            "en": "Aeromodeling",
            "cn": "航模"
        },
        "4": {
            "en": "Animals/pets/dogs",
            "cn": "宠物"
        },
        "5": {
            "en": "Archery",
            "cn": "射箭"
        },
        "6": {
            "en": "Arts",
            "cn": "美术"
        },
        "7": {
            "en": "Aquarium",
            "cn": "水族"
        },
        "8": {
            "en": "Astrology",
            "cn": "占星"
        },
        "9": {
            "en": "Astronomy",
            "cn": "天文"
        },
        "10": {
            "en": "Badminton",
            "cn": "羽毛球"
        },
        "11": {
            "en": "Baseball",
            "cn": "棒球"
        },
        "12": {
            "en": "Base Jumping",
            "cn": "低空跳伞"
        },
        "13": {
            "en": "Basketball",
            "cn": "篮球"
        },
        "14": {
            "en": "Beach tanning",
            "cn": "沙滩日晒"
        },
        "15": {
            "en": "Beachcombing",
            "cn": "沙滩冲浪"
        },
        "16": {
            "en": "Bicycling",
            "cn": "自行车"
        },
        "17": {
            "en": "Bird watching",
            "cn": "观鸟"
        },
        "18": {
            "en": "Board Games",
            "cn": "桌游"
        },
        "19": {
            "en": "Boating",
            "cn": "划船"
        },
        "20": {
            "en": "Body Building",
            "cn": "健身"
        },
        "21": {
            "en": "Bonsai Tree",
            "cn": "盆栽"
        },
        "22": {
            "en": "Boomerangs",
            "cn": "回旋镖"
        },
        "23": {
            "en": "Bowling",
            "cn": "保龄球"
        },
        "24": {
            "en": "Cake Decorating",
            "cn": "糕点装饰"
        },
        "25": {
            "en": "Calligraphy",
            "cn": "书法"
        },
        "26": {
            "en": "Camping",
            "cn": "野营"
        },
        "27": {
            "en": "Canoeing",
            "cn": "独木舟"
        },
        "28": {
            "en": "Cartooning",
            "cn": "画漫画"
        },
        "29": {
            "en": "Car Racing",
            "cn": "飙车"
        },
        "30": {
            "en": "Casino Gambling",
            "cn": "赌场"
        },
        "31": {
            "en": "Ceramics",
            "cn": "陶术"
        },
        "32": {
            "en": "Cheerleading",
            "cn": "啦啦队"
        },
        "33": {
            "en": "Church/church activities",
            "cn": "教会"
        },
        "34": {
            "en": "Coin Collecting",
            "cn": "硬币收集"
        },
        "35": {
            "en": "Collecting",
            "cn": "收集"
        },
        "36": {
            "en": "Collecting Antiques",
            "cn": "股东"
        },
        "37": {
            "en": "Cooking",
            "cn": "烹饪"
        },
        "38": {
            "en": "Cosplay",
            "cn": "角色扮演"
        },
        "39": {
            "en": "Crafts",
            "cn": "手工"
        },
        "40": {
            "en": "Dancing",
            "cn": "跳舞"
        },
        "41": {
            "en": "Darts",
            "cn": "飞镖"
        },
        "42": {
            "en": "Dominoes",
            "cn": "多米诺"
        },
        "43": {
            "en": "Drawing",
            "cn": "画画"
        },
        "44": {
            "en": "Eating out",
            "cn": "吃货"
        },
        "45": {
            "en": "Fishing",
            "cn": "钓鱼"
        },
        "46": {
            "en": "Football",
            "cn": "橄榄球"
        },
        "47": {
            "en": "Gardening",
            "cn": "园艺"
        },
        "48": {
            "en": "Golf",
            "cn": "高尔夫"
        },
        "49": {
            "en": "Go Kart Racing",
            "cn": "卡丁车"
        },
        "50": {
            "en": "Guitar",
            "cn": "吉他"
        },
        "51": {
            "en": "Hiking",
            "cn": "徒步旅行"
        },
        "52": {
            "en": "Horse riding",
            "cn": "骑马"
        },
        "53": {
            "en": "Hunting",
            "cn": "打猎"
        },
        "54": {
            "en": "Iceskating",
            "cn": "滑冰"
        },
        "55": {
            "en": "Jump Roping",
            "cn": "跳绳"
        },
        "56": {
            "en": "Kayaking",
            "cn": "皮船"
        },
        "57": {
            "en": "Kites",
            "cn": "风筝"
        },
        "58": {
            "en": "Legos",
            "cn": "乐高"
        },
        "59": {
            "en": "Listening to music",
            "cn": "听音乐"
        },
        "60": {
            "en": "Magic",
            "cn": "魔术"
        },
        "61": {
            "en": "Marksmanship",
            "cn": "射击"
        },
        "62": {
            "en": "Martial Arts",
            "cn": "武术"
        },
        "63": {
            "en": "Models",
            "cn": "模型"
        },
        "64": {
            "en": "Motorcycles",
            "cn": "摩托党"
        },
        "65": {
            "en": "Mountain Climbing",
            "cn": "爬山"
        },
        "66": {
            "en": "Origami",
            "cn": "折纸"
        },
        "67": {
            "en": "Painting",
            "cn": "油画"
        },
        "68": {
            "en": "Paintball",
            "cn": "彩蛋射击"
        },
        "69": {
            "en": "Parachuting",
            "cn": "跳伞"
        },
        "70": {
            "en": "Parkour",
            "cn": "跑酷"
        },
        "71": {
            "en": "Photography",
            "cn": "摄影"
        },
        "72": {
            "en": "Piano",
            "cn": "钢琴"
        },
        "73": {
            "en": "Rafting",
            "cn": "漂流"
        },
        "74": {
            "en": "Reading",
            "cn": "阅读"
        },
        "75": {
            "en": "Robotics",
            "cn": "机器人"
        },
        "76": {
            "en": "Rockets",
            "cn": "火箭"
        },
        "77": {
            "en": "Running",
            "cn": "跑"
        },
        "78": {
            "en": "Skiing",
            "cn": "滑雪"
        },
        "79": {
            "en": "Shopping",
            "cn": "逛街"
        },
        "80": {
            "en": "Skateboarding",
            "cn": "滑雪板"
        },
        "81": {
            "en": "Sketching",
            "cn": "素描"
        },
        "82": {
            "en": "Sky Diving",
            "cn": "高空跳伞"
        },
        "83": {
            "en": "Sleeping",
            "cn": "睡"
        },
        "84": {
            "en": "Snorkeling",
            "cn": "潜水"
        },
        "85": {
            "en": "Soccer",
            "cn": "足球"
        },
        "86": {
            "en": "Pool",
            "cn": "台球"
        },
        "87": {
            "en": "Socializing",
            "cn": "交友"
        },
        "88": {
            "en": "Stamp Collecting",
            "cn": "集邮"
        },
        "89": {
            "en": "Surfing",
            "cn": "冲浪"
        },
        "90": {
            "en": "Karaoke",
            "cn": "卡拉OK"
        },
        "91": {
            "en": "Swimming",
            "cn": "游泳"
        },
        "92": {
            "en": "Table Tennies",
            "cn": "乒乓球"
        },
        "93": {
            "en": "Tea Tasting",
            "cn": "品茶"
        },
        "94": {
            "en": "Tennis",
            "cn": "网球"
        },
        "95": {
            "en": "Traveling",
            "cn": "旅游"
        },
        "96": {
            "en": "Treasure Hunting",
            "cn": "寻宝"
        },
        "97": {
            "en": "TV watching",
            "cn": "看电视"
        },
        "98": {
            "en": "Ultimate Frisbee",
            "cn": "飞碟"
        },
        "99": {
            "en": "Video Games",
            "cn": "电游"
        },
        "100": {
            "en": "Walking",
            "cn": "走"
        },
        "101": {
            "en": "Wine Tasting",
            "cn": "品酒"
        },
        "102": {
            "en": "Writing",
            "cn": "写作"
        },
        "103": {
            "en": "Yoga",
            "cn": "瑜伽"
        }
    };
};

Interest.prototype.getValueString = function (keyArray, lang) {
    var valueString = '';
    for (var i = 0; i < keyArray.length; i++) {
        var key = keyArray[i];
        if (this.data_mapping[key] != null) {
            valueString += this.data_mapping[key][lang] + ', ';
        }
    }
    if (valueString.length > 0) {
        valueString = valueString.substring(0, valueString.length - 2); // get rid of the last comma
    }
    return valueString;
};

module.exports = Interest;