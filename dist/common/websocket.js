'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var io = require('./wx_socketio.js');

var Socket = function () {
    function Socket(host) {
        _classCallCheck(this, Socket);

        var self = this;

        this.user_id = getApp().$app.globalData.selfUserInfo.email;
        this.wx_token = getApp().$app.globalData.selfUserInfo.wx_token;
        this.host = host;
        this.connected = false;

        this.socketConnect();

        this.attachListeners();
    }

    _createClass(Socket, [{
        key: 'socketConnect',
        value: function socketConnect() {
            this.socket = io.connect(this.host, {
                'forceNew': true,
                'query': 'user_id=' + this.user_id + '&wx_token=' + this.wx_token
            });
        }
    }, {
        key: 'sendMessage',
        value: function sendMessage(recipient_id, message, ackCallback) {
            if (!this.socket.connected) {
                console.log('not connected');
                socketConnect();
                return;
            }
            var payload = {
                "message": message,
                "recipient_id": recipient_id,
                "mode": "PRIVATE_MESSAGE"
            };
            this.socket.emit('direct message', payload, function (data) {
                // callback param is not supported by current wx_socketio implementation
                if (data.success) {
                    console.log('message sent successfully');
                } else {
                    console.log('failed to send the message');
                }
            });
        }
    }, {
        key: 'attachListeners',
        value: function attachListeners() {
            var self = this;

            this.socket.on('connect', function () {
                console.log('websocket connected');
            });

            this.socket.on('disconnect', function () {
                console.log('websocket disconnected');
            });
        }
    }]);

    return Socket;
}();

exports.default = Socket;