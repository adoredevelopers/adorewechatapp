'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _wepy = require('./../npm/wepy/lib/wepy.js');

var _wepy2 = _interopRequireDefault(_wepy);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var utils = require('./../common/utils.js');

var AdoreChatPage = function (_wepy$page) {
    _inherits(AdoreChatPage, _wepy$page);

    function AdoreChatPage() {
        var _ref;

        var _temp, _this, _ret;

        _classCallCheck(this, AdoreChatPage);

        for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
            args[_key] = arguments[_key];
        }

        return _ret = (_temp = (_this = _possibleConstructorReturn(this, (_ref = AdoreChatPage.__proto__ || Object.getPrototypeOf(AdoreChatPage)).call.apply(_ref, [this].concat(args))), _this), _this.config = {
            'navigationBarTitleText': '聊天'
        }, _this.data = {
            'pageName': null,
            'userInfo': null,
            'selfUserInfo': null,
            'chatConversations': [],
            'presentationLayerKVs': {}
        }, _this.methods = {
            touchStart: function touchStart(data, e) {
                this.touchStart = e.changedTouches[0];
                this.$apply();
            },
            touchEnd: function touchEnd(data, e) {
                this.touchEnd = e.changedTouches[0];
                this.$apply();
                this.toggleRemoveButton(data);
            },
            goToUserDetailsPage: function goToUserDetailsPage(data, e) {
                wx.navigateTo({
                    'url': 'user_details_page?userInfoJSONString=' + JSON.stringify(data)
                });
            },
            goToChatMessagePage: function goToChatMessagePage(data, e) {
                wx.navigateTo({
                    'url': 'chat_messages_page?userInfoJSONString=' + JSON.stringify(data)
                });
            }
        }, _temp), _possibleConstructorReturn(_this, _ret);
    }

    _createClass(AdoreChatPage, [{
        key: 'onLoad',
        value: function onLoad(params) {
            var self = this;

            this.pageName = this.$name;

            this.$wxapp.$app.getSelfUserInfo(function (userInfo) {
                self.selfUserInfo = userInfo;
            });
        }
    }, {
        key: 'onShow',
        value: function onShow() {
            var self = this;

            wx.getStorage({
                'key': 'chat_conversations',
                'success': function success(result) {
                    self.chatConversations = result.data;
                    self.$apply();
                },
                'fail': function fail(err) {
                    self.chatConversations = {};
                    self.$apply();
                }
            });
        }
    }, {
        key: 'toggleRemoveButton',
        value: function toggleRemoveButton(conversationId) {
            var self = this;

            var action = utils.resolveSwipeDirection(this.touchStart, this.touchEnd);
            if (action === 'moveLeft') {
                if (this.chatConversations[conversationId].remove_btn_class === 'hidden') {
                    this.chatConversations[conversationId].remove_btn_class = '';
                }
            } else if (action === 'moveRight') {
                if (this.chatConversations[conversationId].remove_btn_class === '') {
                    this.chatConversations[conversationId].remove_btn_class = 'hidden';
                }
            } else {}

            this.$apply();
        }
    }]);

    return AdoreChatPage;
}(_wepy2.default.page);


Page(require('./../npm/wepy/lib/wepy.js').default.$createPage(AdoreChatPage));
