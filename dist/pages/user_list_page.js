'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _wepy = require('./../npm/wepy/lib/wepy.js');

var _wepy2 = _interopRequireDefault(_wepy);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var constants = require('./../common/constants.js');
var utils = require('./../common/utils.js');

var UserListPage = function (_wepy$page) {
    _inherits(UserListPage, _wepy$page);

    function UserListPage() {
        var _ref;

        var _temp, _this, _ret;

        _classCallCheck(this, UserListPage);

        for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
            args[_key] = arguments[_key];
        }

        return _ret = (_temp = (_this = _possibleConstructorReturn(this, (_ref = UserListPage.__proto__ || Object.getPrototypeOf(UserListPage)).call.apply(_ref, [this].concat(args))), _this), _this.config = {
            'navigationBarTitleText': '附近用户',
            'enablePullDownRefresh': true
        }, _this.data = {
            'pageName': null,
            'selfUserInfo': null,
            'filterParameters': {
                'mode': 'normal',
                'batch_size': constants.constants.common.nearby_user_fetch_constants.batch_size,
                'longitude': -79.556561,
                'latitude': 43.796788,
                'max_distance': 100000,
                'min_age': 18,
                'max_age': 40,
                'gender': '', // defaulted to the opposite sex
                'wx_token': ''
            },
            'allowedPagingCount': 5,
            'currentPagingCount': 0,
            'noMorePages': false,
            'nearbyUsers': []
        }, _this.methods = {
            goToUserDetailsPage: function goToUserDetailsPage(data, e) {
                wx.navigateTo({
                    'url': 'user_details_page?userInfoJSONString=' + JSON.stringify(data)
                });
            }
        }, _temp), _possibleConstructorReturn(_this, _ret);
    }

    _createClass(UserListPage, [{
        key: 'onLoad',
        value: function onLoad(params) {
            var self = this;

            this.pageName = this.$name;

            var mode = 'normal';
            this.$wxapp.$app.getSelfUserInfo(function (userInfo) {
                self.selfUserInfo = userInfo;
                if (constants.constants.common.account_types.premium.indexOf(self.selfUserInfo.account_type) != -1) {
                    self.allowedPagingCount = constants.constants.common.nearby_user_fetch_constants.allowed_paging_count_premium;
                } else {
                    self.allowedPagingCount = constants.constants.common.nearby_user_fetch_constants.allowed_paging_count_normal;
                }
                self.fetchNearbyUsers(mode, function (success) {
                    self.refreshCompleted(mode, success);
                });
            });
        }
    }, {
        key: 'onPullDownRefresh',
        value: function onPullDownRefresh() {
            var self = this;

            var mode = 'normal';
            this.fetchNearbyUsers(mode, function (success) {
                self.refreshCompleted(mode, success);
            });
        }
    }, {
        key: 'onReachBottom',
        value: function onReachBottom() {
            var self = this;

            var mode = 'paging';
            if (this.currentPagingCount < this.allowedPagingCount) {
                if (this.noMorePages) {
                    wx.showToast({
                        'title': '没有了',
                        'icon': 'success'
                    });
                } else {
                    this.fetchNearbyUsers(mode, function (success) {
                        self.refreshCompleted(mode, success);
                    });
                }
            } else {
                wx.showToast({
                    'title': '已到达上限',
                    'icon': 'success'
                });
            }
        }
    }, {
        key: 'refreshCompleted',
        value: function refreshCompleted(mode, success) {
            var self = this;

            if (mode === 'paging') {
                wx.hideNavigationBarLoading();
            } else {
                wx.stopPullDownRefresh({
                    'complete': function complete(res) {
                        wx.hideToast();
                    }
                });
            }

            if (success != null && !success) {
                wx.hideToast();
                wx.showModal({
                    'title': '刷新失败',
                    'content': '兜酱在刷新途中被怪蜀黍领走了。。。',
                    'showCancel': false
                });
            }
        }
    }, {
        key: 'fetchNearbyUsers',
        value: function fetchNearbyUsers(mode, cb) {
            var self = this;

            if (mode === 'paging') {
                wx.showNavigationBarLoading();
            } else {
                wx.showToast({
                    'title': '载入中...',
                    'icon': 'loading',
                    'mask': true,
                    'duration': 15000
                });
            }
            wx.getStorage({
                'key': 'wx_token',
                'success': function success(result) {
                    console.log(result);
                    self.filterParameters.wx_token = result.data;
                    _wepy2.default.getLocation({
                        success: function success(res) {
                            console.log('location: ' + res);
                            sendGetNearbyUserRequest(result.data, res, mode, cb);
                        },
                        fail: function fail(err) {
                            console.log(err);
                            sendGetNearbyUserRequest(result.data, null, mode, cb);
                        }
                    });
                }
            });

            function sendGetNearbyUserRequest(wx_token, coords, mode, cb) {
                self.filterParameters.mode = mode;
                if (coords != null) {
                    self.filterParameters.longitude = coords.longitude;
                    self.filterParameters.latitude = coords.latitude;
                }
                self.$apply();

                utils.sendAPIRequest({
                    'apiName': 'get_nearby_users',
                    'queryString': utils.buildRequestQueryString(self.filterParameters),
                    'requestContentType': 'application/json',
                    'requestData': self.filterParameters
                }, function (res) {
                    var success = res.data.success;
                    var nearbyUsers = res.data.data;
                    if (success) {
                        console.log(nearbyUsers);
                        if (nearbyUsers.length < self.filterParameters.batch_size) {
                            self.noMorePages = true;
                        }
                        nearbyUsers = nearbyUsers.map(utils.normalizeUserInfo);
                        if (self.filterParameters.mode === 'paging') {
                            self.nearbyUsers = self.nearbyUsers.concat(nearbyUsers);
                            self.currentPagingCount = self.currentPagingCount + 1;
                        } else {
                            self.nearbyUsers = nearbyUsers;
                            self.currentPagingCount = 0;
                            self.noMorePages = false;
                        }
                        self.$apply();
                    }
                    cb && cb(true);
                }, function (err) {
                    console.log(err);
                    cb && cb(false);
                });
            }
        }
    }]);

    return UserListPage;
}(_wepy2.default.page);


Page(require('./../npm/wepy/lib/wepy.js').default.$createPage(UserListPage));
