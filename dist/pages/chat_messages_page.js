'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _wepy = require('./../npm/wepy/lib/wepy.js');

var _wepy2 = _interopRequireDefault(_wepy);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var utils = require('./../common/utils.js');

var ChatMessagePage = function (_wepy$page) {
    _inherits(ChatMessagePage, _wepy$page);

    function ChatMessagePage() {
        var _ref;

        var _temp, _this, _ret;

        _classCallCheck(this, ChatMessagePage);

        for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
            args[_key] = arguments[_key];
        }

        return _ret = (_temp = (_this = _possibleConstructorReturn(this, (_ref = ChatMessagePage.__proto__ || Object.getPrototypeOf(ChatMessagePage)).call.apply(_ref, [this].concat(args))), _this), _this.config = {
            'navigationBarTitleText': '聊天'
        }, _this.data = {
            'pageName': null,
            'userInfo': null,
            'selfUserInfo': null,
            'chatMessages': [],
            'inputChatMessage': '',
            'presentationLayerKVs': {
                'scrollToElementId': ''
            },
            'refPageName': null
        }, _this.methods = {
            hideKeyboard: function hideKeyboard() {
                wx.hideKeyboard();
            },
            showSmiley: function showSmiley(data, e) {
                wx.showToast({
                    'title': 'Coming soon!',
                    'icon': 'success'
                });
            },
            updateInputChatMessage: function updateInputChatMessage(data, e) {
                this.inputChatMessage = e.detail.value;
            },
            sendMessage: function sendMessage(data, e) {
                var message = this.inputChatMessage;
                this.$wxapp.$app.socket.sendMessage(this.userInfo.email, message, function (ackData) {
                    console.log(ackData);
                });
                var messageRecord = {
                    'sender_id': this.selfUserInfo.email,
                    'recipient_id': this.userInfo.email,
                    'message': message,
                    'timestamp': Date.now()
                };
                this.chatMessages.push(messageRecord);
                this.updateStoredChatMessages();
                this.inputChatMessage = '';
                this.$apply();
            },
            goToUserDetailsPage: function goToUserDetailsPage(data, e) {
                var self = this;

                var pageName = 'user_details_page';
                if (pageName === self.refPageName) {
                    wx.navigateBack();
                } else {
                    wx.navigateTo({
                        'url': 'user_details_page?ref=chat_message_page&userInfoJSONString=' + JSON.stringify(self.userInfo)
                    });
                }
            }
        }, _temp), _possibleConstructorReturn(_this, _ret);
    }

    _createClass(ChatMessagePage, [{
        key: 'onLoad',
        value: function onLoad(params) {
            var self = this;

            this.pageName = this.$name;

            this.refPageName = params.ref;
            this.userInfo = JSON.parse(params.userInfoJSONString);
            wx.getStorage({
                'key': 'chat_conversations',
                'success': function success(result) {
                    var chatConversations = result.data;
                    if (chatConversations[self.userInfo.email] != null) {
                        self.chatMessages = chatConversations[self.userInfo.email].messages;
                    } else {
                        // self.chatMessages = [];
                        var requestData = {
                            'wx_token': self.selfUserInfo.wx_token,
                            'conversation_user_id': self.userInfo.email
                        };
                        utils.sendAPIRequest({
                            'apiName': 'get_user_conversation',
                            'queryString': utils.buildRequestQueryString(requestData),
                            'requestContentType': 'application/json',
                            'requestData': requestData
                        }, function (res) {
                            self.chatMessages = res.data.data;
                            self.$apply();
                            if (self.chatMessages.length > 0) {
                                self.updateStoredChatMessages();
                            }
                        }, function (err) {
                            self.chatMessages = [];
                            self.$apply();
                        }, null);
                    }
                },
                'fail': function fail(err) {
                    console.log(err);
                    wx.setStorage({
                        'key': 'chat_conversations',
                        'data': {}
                    });
                    self.chatMessages = [];
                }
            });

            wx.setNavigationBarTitle({
                'title': this.userInfo.normalized_display_name
            });

            this.$wxapp.$app.getSelfUserInfo(function (userInfo) {
                self.selfUserInfo = userInfo;
            });

            this.$wxapp.$app.socket.socket.on('private message', function (data) {
                delete data['mode'];
                delete data['socket_message_id'];
                self.chatMessages.push(data);
                self.updateStoredChatMessages();
            });
        }
    }, {
        key: 'onReady',
        value: function onReady() {
            var self = this;

            this.scrollToBottom();
        }
    }, {
        key: 'updateStoredChatMessages',
        value: function updateStoredChatMessages() {
            var self = this;

            wx.getStorage({
                'key': 'chat_conversations',
                'success': function success(result) {
                    var chatConversations = result.data;
                    for (var idx in self.chatMessages) {
                        self.chatMessages[idx].date_time_string = utils.getReadableDateTimeString(self.chatMessages[idx].timestamp, false);
                    }
                    if (chatConversations[self.userInfo.email] == null) {
                        chatConversations[self.userInfo.email] = {
                            'user_info': self.userInfo,
                            'remove_btn_class': 'hidden',
                            'new_message_class': 'hidden',
                            'messages': self.chatMessages
                        };
                    } else {
                        chatConversations[self.userInfo.email].messages = self.chatMessages;
                    }
                    wx.setStorage({
                        'key': 'chat_conversations',
                        'data': chatConversations,
                        'success': function success() {
                            self.scrollToBottom();
                        }
                    });
                }
            });
        }
    }, {
        key: 'scrollToBottom',
        value: function scrollToBottom() {
            // 1. this is equal to $(document).ready(); scroll-top, scroll-into-view value has to be changed here to make it work
            // 2. assigning to '' empty first, not sure why it has this behaviour
            this.presentationLayerKVs.scrollToElementId = '';
            this.$apply();
            this.presentationLayerKVs.scrollToElementId = 'bottom-anchor';
            this.$apply();
        }
    }]);

    return ChatMessagePage;
}(_wepy2.default.page);


Page(require('./../npm/wepy/lib/wepy.js').default.$createPage(ChatMessagePage));
