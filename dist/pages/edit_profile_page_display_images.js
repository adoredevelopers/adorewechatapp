'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _wepy = require('./../npm/wepy/lib/wepy.js');

var _wepy2 = _interopRequireDefault(_wepy);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var constants = require('./../common/constants.js');
var utils = require('./../common/utils.js');

var EditProfilePageDisplayImages = function (_wepy$page) {
    _inherits(EditProfilePageDisplayImages, _wepy$page);

    function EditProfilePageDisplayImages() {
        var _ref;

        var _temp, _this, _ret;

        _classCallCheck(this, EditProfilePageDisplayImages);

        for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
            args[_key] = arguments[_key];
        }

        return _ret = (_temp = (_this = _possibleConstructorReturn(this, (_ref = EditProfilePageDisplayImages.__proto__ || Object.getPrototypeOf(EditProfilePageDisplayImages)).call.apply(_ref, [this].concat(args))), _this), _this.config = {
            'navigationBarTitleText': '修改照片'
        }, _this.data = {
            'pageName': null,
            'wx_token': null,
            'displayImageUrls': [],
            'presentationLayerKVs': {
                'normalizedDisplayImageUrls': []
            },
            'hasUploadImage': false,
            'hasChangedImage': false
        }, _this.methods = {
            chooseImageForUpload: function chooseImageForUpload(data, e) {
                var self = this;

                wx.chooseImage({
                    'count': 1, // 默认9
                    // 'sizeType': ['original', 'compressed'], // 可以指定是原图还是压缩图，默认二者都有
                    'sizeType': ['compressed'],
                    'sourceType': ['album', 'camera'], // 可以指定来源是相册还是相机，默认二者都有
                    'success': function success(res) {
                        console.log(res);
                        // 返回选定照片的本地文件路径列表，tempFilePath可以作为img标签的src属性显示图片
                        var imagePlacementIndex = data;
                        var imageTempPath = res.tempFilePaths[0];
                        self.displayImageUrls[imagePlacementIndex] = imageTempPath;
                        self.presentationLayerKVs.normalizedDisplayImageUrls[imagePlacementIndex] = imageTempPath;
                        self.hasUploadImage = true;
                        self.$apply();
                    },
                    'fail': function fail(err) {
                        console.log(err);
                    },
                    'complete': function complete() {}
                });
            },
            touchStart: function touchStart(data, e) {
                var self = this;
                this.touchStart = e.changedTouches[0];
                this.$apply();
            },
            touchEnd: function touchEnd(data, e) {
                console.log(data);
                var self = this;
                this.touchEnd = e.changedTouches[0];
                this.$apply();
                this.rearrangeImages(data);
            },
            deleteImage: function deleteImage(data, e) {
                console.log(data);
                this.displayImageUrls.splice(data, 1);
                this.presentationLayerKVs.normalizedDisplayImageUrls.splice(data, 1);
                this.hasChangedImage = true;
                this.$apply();
            }
        }, _temp), _possibleConstructorReturn(_this, _ret);
    }

    _createClass(EditProfilePageDisplayImages, [{
        key: 'onLoad',
        value: function onLoad(params) {
            var self = this;

            this.pageName = this.$name;

            this.wx_token = params.wx_token;
            this.$wxapp.$app.getSelfUserInfo(function (userInfo) {
                self.presentationLayerKVs.normalizedDisplayImageUrls = userInfo.normalized_display_image_urls;
                self.displayImageUrls = userInfo.display_image_urls;
            });
        }
    }, {
        key: 'rearrangeImages',
        value: function rearrangeImages(imageIndex) {
            var self = this;

            var action = utils.resolveSwipeDirection(this.touchStart, this.touchEnd);

            // determine swapping indexes
            var displayImageUrlArrayLength = this.displayImageUrls.length;
            if (imageIndex === 0) {
                if (action === 'moveRight' && displayImageUrlArrayLength > 1) {
                    this.swapDisplayImageUrls(0, 1);
                } else if (action === 'moveDown' && displayImageUrlArrayLength > 3) {
                    this.swapDisplayImageUrls(0, 3);
                } else {}
            } else if (imageIndex === 1) {
                if (action === 'moveLeft') {
                    this.swapDisplayImageUrls(1, 0);
                } else if (action === 'moveDown' && displayImageUrlArrayLength > 2) {
                    this.swapDisplayImageUrls(1, 2);
                } else {}
            } else if (imageIndex === 2) {
                if (action === 'moveLeft') {
                    this.swapDisplayImageUrls(2, 0);
                } else if (action === 'moveDown' && displayImageUrlArrayLength > 5) {
                    this.swapDisplayImageUrls(2, 5);
                } else if (action === 'moveUp') {
                    this.swapDisplayImageUrls(2, 1);
                } else {}
            } else if (imageIndex === 3) {
                if (action === 'moveRight' && displayImageUrlArrayLength > 4) {
                    this.swapDisplayImageUrls(3, 4);
                } else if (action === 'moveDown' && displayImageUrlArrayLength > 6) {
                    this.swapDisplayImageUrls(3, 6);
                } else if (action === 'moveUp') {
                    this.swapDisplayImageUrls(3, 0);
                } else {}
            } else if (imageIndex === 4) {
                if (action === 'moveRight' && displayImageUrlArrayLength > 5) {
                    this.swapDisplayImageUrls(4, 5);
                } else if (action === 'moveLeft') {
                    this.swapDisplayImageUrls(4, 3);
                } else if (action === 'moveDown' && displayImageUrlArrayLength > 7) {
                    this.swapDisplayImageUrls(4, 7);
                } else if (action === 'moveUp') {
                    this.swapDisplayImageUrls(4, 0);
                } else {}
            } else if (imageIndex === 5) {
                if (action === 'moveLeft') {
                    this.swapDisplayImageUrls(5, 4);
                } else if (action === 'moveUp') {
                    this.swapDisplayImageUrls(5, 2);
                } else {}
            } else if (imageIndex === 6) {
                if (action === 'moveRight' && displayImageUrlArrayLength > 7) {
                    this.swapDisplayImageUrls(6, 7);
                } else if (action === 'moveUp') {
                    this.swapDisplayImageUrls(6, 3);
                } else {}
            } else if (imageIndex === 7) {
                if (action === 'moveLeft') {
                    this.swapDisplayImageUrls(7, 6);
                } else if (action === 'moveUp') {
                    this.swapDisplayImageUrls(7, 4);
                } else {}
            }
        }
    }, {
        key: 'swapDisplayImageUrls',
        value: function swapDisplayImageUrls(fromIndex, toIndex) {
            var self = this;

            // for displayImageUrls, needed for upload/update to server
            var fromImage = this.displayImageUrls[fromIndex];
            this.displayImageUrls[fromIndex] = this.displayImageUrls[toIndex];
            this.displayImageUrls[toIndex] = fromImage;
            // for normalizedDisplayImageUrls
            fromImage = this.presentationLayerKVs.normalizedDisplayImageUrls[fromIndex];
            this.presentationLayerKVs.normalizedDisplayImageUrls[fromIndex] = this.presentationLayerKVs.normalizedDisplayImageUrls[toIndex];
            this.presentationLayerKVs.normalizedDisplayImageUrls[toIndex] = fromImage;
            self.hasChangedImage = true;
            this.$apply();
        }
    }, {
        key: 'uploadAndSaveDisplayImageChanges',
        value: function uploadAndSaveDisplayImageChanges() {
            var self = this;

            wx.showToast({
                'title': '更新中。。。',
                'icon': 'loading',
                'mask': true,
                'duration': 15000

            });

            if (self.hasUploadImage) {
                var apiUrl = constants.getApiUrl(constants.getEnv(), 'update_user_display_images') + '?wx_token=' + self.wx_token; //wx.getStorageSync('wx_token');
                utils.uploadAndUpdateUserDisplayImages(this, apiUrl, this.displayImageUrls, function (err, newDisplayImageUrls) {
                    if (err) {
                        console.log(err);
                        wx.hideToast();
                        wx.showModal({
                            'title': '更新失败',
                            'content': '更新中遇到了问题，兜兜酱正在排查中。。。',
                            'showCancel': false
                        });
                    } else {
                        self.displayImageUrls = newDisplayImageUrls;
                        self.presentationLayerKVs.normalizedDisplayImageUrls = utils.normalizeDisplayImageUrls(self.displayImageUrls);
                        self.$wxapp.$app.globalData.selfUserInfo.display_image_urls = self.displayImageUrls;
                        self.$wxapp.$app.globalData.selfUserInfo.normalized_display_image_urls = self.presentationLayerKVs.normalizedDisplayImageUrls;
                        self.$apply();
                        wx.hideToast();
                        wx.showToast({
                            'title': '更新成功',
                            'icon': 'success'
                        });
                    }
                });
            } else if (self.hasChangedImage) {
                utils.sendAPIRequest({
                    'apiName': 'update_user_fields',
                    'queryString': '?wx_token=' + this.wx_token, //wx.getStorageSync('wx_token'),
                    'requestContentType': 'application/json',
                    'requestData': {
                        'display_image_urls': this.displayImageUrls
                    }
                }, function (res) {
                    if (!res.data.success) {
                        wx.hideToast();
                        wx.showModal({
                            'title': '更新失败',
                            'content': '更新中遇到了问题，兜兜酱正在排查中。。。',
                            'showCancel': false
                        });
                    } else {
                        var updatedUserInfo = res.data.data;
                        var updatedDisplayImageUrls = updatedUserInfo.display_image_urls;
                        self.displayImageUrls = updatedDisplayImageUrls;
                        self.presentationLayerKVs.normalizedDisplayImageUrls = utils.normalizeDisplayImageUrls(self.displayImageUrls);
                        self.$wxapp.$app.globalData.selfUserInfo.display_image_urls = self.displayImageUrls;
                        self.$wxapp.$app.globalData.selfUserInfo.normalized_display_image_urls = self.presentationLayerKVs.normalizedDisplayImageUrls;
                        self.$apply();
                        wx.hideToast();
                        wx.showToast({
                            'title': '更新成功',
                            'icon': 'success'
                        });
                    }
                }, function (err) {
                    console.log(err);
                    wx.hideToast();
                    wx.showModal({
                        'title': '更新失败',
                        'content': '更新中遇到了问题，兜兜酱正在排查中。。。',
                        'showCancel': false
                    });
                });
            }
        }
    }]);

    return EditProfilePageDisplayImages;
}(_wepy2.default.page);


Page(require('./../npm/wepy/lib/wepy.js').default.$createPage(EditProfilePageDisplayImages));
