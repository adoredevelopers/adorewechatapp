'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _wepy = require('./../npm/wepy/lib/wepy.js');

var _wepy2 = _interopRequireDefault(_wepy);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var moment = require('./../npm/moment/moment.js');

var constants = require('./../common/constants.js');
var utils = require('./../common/utils.js');

var EditProfilePage = function (_wepy$page) {
    _inherits(EditProfilePage, _wepy$page);

    function EditProfilePage() {
        var _ref;

        var _temp, _this, _ret;

        _classCallCheck(this, EditProfilePage);

        for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
            args[_key] = arguments[_key];
        }

        return _ret = (_temp = (_this = _possibleConstructorReturn(this, (_ref = EditProfilePage.__proto__ || Object.getPrototypeOf(EditProfilePage)).call.apply(_ref, [this].concat(args))), _this), _this.config = {
            'navigationBarTitleText': '修改信息'
        }, _this.data = {
            'pageName': null,
            'selfUserInfo': null,
            'todayDate': null,
            'birthdayMaxDate': null,
            'birthdayMaxDateString': null,
            'birthdayMinDate': null,
            'birthdayMinDateString': null,
            'lastOpenedExpandViewFieldName': null,
            'presentationLayerKVs': {
                'user_info_fields': {
                    'basic_info_fields': {},
                    'detailed_info_fields': {},
                    'more_info_fields': {}
                }
            }
        }, _this.methods = {
            showMultiSelectExpandView: function showMultiSelectExpandView(data, e) {
                console.log(e);
                var dataSet = e.currentTarget.dataset;
                var infoFields = this.presentationLayerKVs.user_info_fields[dataSet.userInfoSection];
                if (this.lastOpenedExpandViewFieldName != null) {
                    infoFields[this.lastOpenedExpandViewFieldName].expand_view_hidden_class = 'hidden';
                }
                if (dataSet.infoFieldKey !== this.lastOpenedExpandViewFieldName) {
                    infoFields[dataSet.infoFieldKey].expand_view_hidden_class = '';
                    this.lastOpenedExpandViewFieldName = dataSet.infoFieldKey;
                } else {
                    this.lastOpenedExpandViewFieldName = null;
                }
            },
            birthdayPickerChange: function birthdayPickerChange(data, e) {
                console.log(e);
                var birthday = moment(e.detail.value);
                var infoFields = this.presentationLayerKVs.user_info_fields[e.currentTarget.dataset.userInfoSection];
                infoFields[e.currentTarget.dataset.infoFieldKey].momentDate = birthday;
                if (e.detail.value !== infoFields[e.currentTarget.dataset.infoFieldKey].text) {
                    infoFields[e.currentTarget.dataset.infoFieldKey].text = e.detail.value;
                    infoFields[e.currentTarget.dataset.infoFieldKey].changed = true;
                }
            },
            pickerChange: function pickerChange(data, e) {
                console.log(e);
                var infoFields = this.presentationLayerKVs.user_info_fields[e.currentTarget.dataset.userInfoSection];
                if (e.detail.value !== infoFields[e.currentTarget.dataset.infoFieldKey].pickedIndex) {
                    infoFields[e.currentTarget.dataset.infoFieldKey].pickedIndex = e.detail.value;
                    infoFields[e.currentTarget.dataset.infoFieldKey].val = infoFields[e.currentTarget.dataset.infoFieldKey].pickerRange.keys[e.detail.value];
                    infoFields[e.currentTarget.dataset.infoFieldKey].text = infoFields[e.currentTarget.dataset.infoFieldKey].pickerRange.values[e.detail.value].cn;
                    infoFields[e.currentTarget.dataset.infoFieldKey].changed = true;
                }
            },
            multiSelectCheckboxGroupChange: function multiSelectCheckboxGroupChange(data, e) {
                console.log(e);
                var multiSelectionChanged = false;
                var infoFields = this.presentationLayerKVs.user_info_fields[e.currentTarget.dataset.userInfoSection];
                if (e.detail.value.length === infoFields[e.currentTarget.dataset.infoFieldKey].val_array.length) {
                    for (var val in e.detail.value) {
                        if (infoFields[e.currentTarget.dataset.infoFieldKey].val_array.indexOf(val) === -1) {
                            multiSelectionChanged = true;
                            break;
                        }
                    }
                } else {
                    multiSelectionChanged = true;
                }
                if (multiSelectionChanged) {
                    var staticMappingObject = null;
                    switch (e.currentTarget.dataset.infoFieldKey) {
                        case 'interest':
                            staticMappingObject = new constants.Interest();
                            break;
                        case 'language':
                            staticMappingObject = new constants.Language();
                            break;
                        default:
                            break;
                    }
                    infoFields[e.currentTarget.dataset.infoFieldKey].val_array.map(function (key) {
                        infoFields[e.currentTarget.dataset.infoFieldKey].multiSelectKVs[key].checked = false;
                    });
                    infoFields[e.currentTarget.dataset.infoFieldKey].val_array = e.detail.value;
                    infoFields[e.currentTarget.dataset.infoFieldKey].val_array.map(function (key) {
                        infoFields[e.currentTarget.dataset.infoFieldKey].multiSelectKVs[key].checked = true;
                    });
                    infoFields[e.currentTarget.dataset.infoFieldKey].text = staticMappingObject.getValueString(e.detail.value, 'cn');
                    infoFields[e.currentTarget.dataset.infoFieldKey].changed = true;
                }
            },
            textFieldUpdate: function textFieldUpdate(data, e) {
                console.log(e);
                var infoFields = this.presentationLayerKVs.user_info_fields[e.currentTarget.dataset.userInfoSection];
                if (e.detail.value !== infoFields[e.currentTarget.dataset.infoFieldKey].text) {
                    infoFields[e.currentTarget.dataset.infoFieldKey].text = e.detail.value;
                    infoFields[e.currentTarget.dataset.infoFieldKey].changed = true;
                }
            },
            textAreaUpdate: function textAreaUpdate(data, e) {
                console.log(e);
                var infoFields = this.presentationLayerKVs.user_info_fields[e.currentTarget.dataset.userInfoSection];
                if (e.detail.value !== infoFields[e.currentTarget.dataset.infoFieldKey].text) {
                    infoFields[e.currentTarget.dataset.infoFieldKey].text = e.detail.value;
                    infoFields[e.currentTarget.dataset.infoFieldKey].changed = true;
                }
            },
            saveUserProfileInfo: function saveUserProfileInfo(data, e) {
                var self = this;

                console.log(e);
                var basicInfo = this.presentationLayerKVs.user_info_fields['basic_info_fields'];
                var detailedInfo = this.presentationLayerKVs.user_info_fields['detailed_info_fields'];
                var moreInfo = this.presentationLayerKVs.user_info_fields['more_info_fields'];
                // populate request data
                var updatedUserInfo = {
                    'profile': {},
                    'more_info': {}
                };
                for (var key in basicInfo) {
                    if (basicInfo[key].changed) {
                        if (basicInfo[key].input_type === 'TEXTFIELD') {
                            updatedUserInfo.profile[key] = basicInfo[key].text;
                        } else if (basicInfo[key].input_type === 'PICKER') {
                            if (key === 'gender') {
                                updatedUserInfo.profile[key] = basicInfo[key].val;
                            } else if (key === 'birthday') {
                                updatedUserInfo.profile[key] = basicInfo[key].momentDate.toDate();
                            } else {
                                updatedUserInfo.profile[key] = basicInfo[key].val;
                            }
                        }
                    }
                }
                for (var _key2 in detailedInfo) {
                    if (detailedInfo[_key2].changed) {
                        if (detailedInfo[_key2].input_type === 'TEXTFIELD') {
                            updatedUserInfo.profile[_key2] = detailedInfo[_key2].text;
                        } else if (detailedInfo[_key2].input_type === 'PICKER') {
                            updatedUserInfo.profile[_key2] = detailedInfo[_key2].val;
                        } else if (detailedInfo[_key2].input_type === 'MULTISELECT') {
                            updatedUserInfo.profile[_key2] = detailedInfo[_key2].val_array;
                        }
                    }
                }
                for (var _key3 in moreInfo) {
                    if (moreInfo[_key3].changed) {
                        // any field under 'more info' section is assumed to be of input type 'TEXTAREA'
                        updatedUserInfo.more_info[_key3] = moreInfo[_key3].text;
                    }
                }
                // send request
                wx.showToast({
                    'title': '更新中。。。',
                    'icon': 'loading',
                    'mask': true,
                    'duration': 60000 // 1 minute till timeout and automatically hide the toast
                });
                utils.sendAPIRequest({
                    'apiName': 'update_user_profile',
                    'queryString': '?wx_token=' + self.selfUserInfo.wx_token,
                    'requestContentType': 'application/json',
                    'requestData': {
                        'updatedUserInfo': updatedUserInfo
                    }
                }, function (res) {
                    var updatedUserInfo = res.data.data;
                    updatedUserInfo = utils.normalizeUserInfo(updatedUserInfo);
                    getApp().$app.setSelfUserInfo(updatedUserInfo);
                    wx.hideToast();
                    wx.showToast({
                        'title': '更新成功',
                        'icon': 'success',
                        'mask': true
                    });
                    wx.navigateBack();
                }, function (err) {
                    console.log(err);
                    wx.hideToast();
                    wx.showModal({
                        'title': '更新失败',
                        'content': '你的更新信息被一群单身狗中途劫持，兜叔正在追赶。。。',
                        'showCancel': false
                    });
                });
            }
        }, _temp), _possibleConstructorReturn(_this, _ret);
    }

    _createClass(EditProfilePage, [{
        key: 'onLoad',
        value: function onLoad(params) {
            var self = this;

            this.pageName = this.$name;

            this.todayDate = moment();
            this.birthdayMaxDate = this.todayDate.subtract(17, 'years');
            this.birthdayMaxDateString = this.birthdayMaxDate.year() + '-' + (this.birthdayMaxDate.month() + 1) + '-' + this.birthdayMaxDate.date();
            this.birthdayMinDate = this.todayDate.subtract(100, 'years');
            this.birthdayMinDateString = this.birthdayMinDate.year() + '-' + (this.birthdayMinDate.month() + 1) + '-' + this.birthdayMinDate.date();
        }
    }, {
        key: 'onShow',
        value: function onShow() {
            var self = this;

            this.presentationLayerKVs.user_info_fields = constants.getUserInfoFields();
            this.$wxapp.$app.getSelfUserInfo(function (userInfo) {
                self.selfUserInfo = userInfo;
                self.populateUserInfoFieldsWithSelfUserInfo();
            });
        }
    }, {
        key: 'populateUserInfoFieldsWithSelfUserInfo',
        value: function populateUserInfoFieldsWithSelfUserInfo() {
            var self = this;

            var userInfoFields = this.presentationLayerKVs.user_info_fields;
            var basicInfoFields = userInfoFields.basic_info_fields;
            var detailedInfoFields = userInfoFields.detailed_info_fields;
            var moreInfoFields = userInfoFields.more_info_fields;

            var selfUserBasicInfo = this.selfUserInfo.basicInfo;
            var selfUserDetailedInfo = this.selfUserInfo.detailedInfo;
            var selfUserMoreInfo = this.selfUserInfo.moreInfo;

            for (var key in basicInfoFields) {
                if (selfUserBasicInfo[key] != null) {
                    if (selfUserBasicInfo[key].val != null) {
                        basicInfoFields[key].val = selfUserBasicInfo[key].val;
                    }
                    if (key === 'birthday') {
                        basicInfoFields[key].momentDate = selfUserBasicInfo[key].momentDate;
                    }
                    basicInfoFields[key].text = selfUserBasicInfo[key].text;
                }
                var staticMappingObject = null;
                switch (key) {
                    case 'gender':
                        staticMappingObject = new constants.Gender();
                        break;
                    case 'c_zodiac':
                        staticMappingObject = new constants.ChineseZodiac();
                        break;
                    case 'zodiac':
                        staticMappingObject = new constants.Zodiac();
                        break;
                    case 'height':
                        staticMappingObject = new constants.Height();
                        break;
                    case 'weight':
                        staticMappingObject = new constants.Weight();
                        break;
                    case 'martial_status':
                        staticMappingObject = new constants.MartialStatus();
                        break;
                    default:
                        break;
                }
                if (staticMappingObject != null) {
                    basicInfoFields[key].pickerRange = this.getStaticMappingForPickerRange(staticMappingObject);
                    if (selfUserBasicInfo[key].val == null) {
                        var pickedIndex = -1;
                    } else {
                        var pickedIndex = basicInfoFields[key].pickerRange.keys.indexOf(selfUserBasicInfo[key].val.toString());
                        if (pickedIndex !== -1) {
                            basicInfoFields[key].pickerRange.values[pickedIndex].checked = true;
                        }
                    }
                    basicInfoFields[key].pickedIndex = pickedIndex;
                }
            }
            for (var _key4 in detailedInfoFields) {
                if (selfUserDetailedInfo[_key4] != null) {
                    if (selfUserDetailedInfo[_key4].val != null) {
                        detailedInfoFields[_key4].val = selfUserDetailedInfo[_key4].val;
                    }
                    if (selfUserDetailedInfo[_key4].val_array != null) {
                        detailedInfoFields[_key4].val_array = selfUserDetailedInfo[_key4].val_array;
                    }
                    detailedInfoFields[_key4].text = selfUserDetailedInfo[_key4].text;
                }
                var staticMappingObject = null;
                switch (_key4) {
                    case 'personality':
                        staticMappingObject = new constants.Personality();
                        break;
                    case 'interest':
                        staticMappingObject = new constants.Interest();
                        break;
                    case 'body_shape':
                        staticMappingObject = new constants.BodyShape();
                        break;
                    case 'education_level':
                        staticMappingObject = new constants.EducationLevel();
                        break;
                    case 'occupation':
                        staticMappingObject = new constants.Occupation();
                        break;
                    case 'smoking':
                        staticMappingObject = new constants.Smoking();
                        break;
                    case 'drinking':
                        staticMappingObject = new constants.Drinking();
                        break;
                    case 'residential_status':
                        staticMappingObject = new constants.ResidentialStatus();
                        break;
                    case 'language':
                        staticMappingObject = new constants.Language();
                        break;
                    case 'religion':
                        staticMappingObject = new constants.Religion();
                        break;
                    case 'race':
                        staticMappingObject = new constants.Race();
                        break;
                    case 'income':
                        staticMappingObject = new constants.Income();
                        break;
                    default:
                        break;
                }
                if (staticMappingObject != null) {
                    if (selfUserDetailedInfo[_key4].val != null) {
                        detailedInfoFields[_key4].pickerRange = this.getStaticMappingForPickerRange(staticMappingObject);
                        var pickedIndex = detailedInfoFields[_key4].pickerRange.keys.indexOf(selfUserDetailedInfo[_key4].val.toString());
                        if (pickedIndex != -1) {
                            detailedInfoFields[_key4].pickerRange.values[pickedIndex].checked = true;
                            detailedInfoFields[_key4].pickedIndex = pickedIndex;
                        }
                    } else if (selfUserDetailedInfo[_key4].val_array != null) {
                        detailedInfoFields[_key4].multiSelectKVs = staticMappingObject.data_mapping;
                        for (var i = 0; i < selfUserDetailedInfo[_key4].val_array.length; i++) {
                            var k = selfUserDetailedInfo[_key4].val_array[i];
                            detailedInfoFields[_key4].multiSelectKVs[k].checked = true;
                        }
                    } else {}
                }
            }
            for (var _key5 in moreInfoFields) {
                if (selfUserMoreInfo[_key5] != null) {
                    moreInfoFields[_key5].text = selfUserMoreInfo[_key5].text;
                }
            }
        }
    }, {
        key: 'getStaticMappingForPickerRange',
        value: function getStaticMappingForPickerRange(staticMappingObject) {
            var self = this;

            var pickerRangeKeys = Object.keys(staticMappingObject.data_mapping);
            var pickerRange = {
                'keys': pickerRangeKeys,
                'values': pickerRangeKeys.map(function (key) {
                    return staticMappingObject.data_mapping[key];
                })
            };
            return pickerRange;
        }
    }]);

    return EditProfilePage;
}(_wepy2.default.page);


Page(require('./../npm/wepy/lib/wepy.js').default.$createPage(EditProfilePage));
