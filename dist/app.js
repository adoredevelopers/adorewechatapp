'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _wepy = require('./npm/wepy/lib/wepy.js');

var _wepy2 = _interopRequireDefault(_wepy);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var constants = require('./common/constants.js');
var utils = require('./common/utils.js');
var Socket = require('./common/websocket.js');

var _class = function (_wepy$app) {
    _inherits(_class, _wepy$app);

    function _class() {
        var _ref;

        var _temp, _this, _ret;

        _classCallCheck(this, _class);

        for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
            args[_key] = arguments[_key];
        }

        return _ret = (_temp = (_this = _possibleConstructorReturn(this, (_ref = _class.__proto__ || Object.getPrototypeOf(_class)).call.apply(_ref, [this].concat(args))), _this), _this.config = {
            'pages': ['pages/__splash_loading_page', 'pages/user_list_page', 'pages/user_details_page', 'pages/user_details_page_display_images', 'pages/my_profile_page', 'pages/edit_profile_page', 'pages/edit_profile_page_display_images', 'pages/adore_chat_page', 'pages/chat_messages_page'
            // 'pages/user_questions_page/index',
            ],
            'window': {
                'navigationBarTitleText': '爱兜',
                'navigationBarBackgroundColor': '#d34a4a',
                'navigationBarTextStyle': '#fff',
                'backgroundColor': '#fbf9fe',
                'backgroundTextStyle': 'light'
            },
            'tabBar': {
                'backgroundColor': '#fff',
                'borderStyle': 'white',
                'color': '#222',
                'selectedColor': '#d34a4a',
                'list': [{
                    'pagePath': 'pages/user_list_page',
                    'text': '附近',
                    'iconPath': 'images/icons/marker-outline.png',
                    'selectedIconPath': 'images/icons/marker-color-outline.png'
                }, {
                    'pagePath': 'pages/adore_chat_page',
                    'text': '聊天',
                    'iconPath': 'images/icons/chat-outline.png',
                    'selectedIconPath': 'images/icons/chat-color-outline.png'
                }, {
                    'pagePath': 'pages/my_profile_page',
                    'text': '我',
                    'iconPath': 'images/icons/user-outline.png',
                    'selectedIconPath': 'images/icons/user-color-outline.png'
                }]
            },
            'networkTimeout': {
                // 'request': 10000,
                // 'downloadFile': 10000
            },
            'debug': true
        }, _this.globalData = {
            'selfUserInfo': null,
            'socket': null
        }, _temp), _possibleConstructorReturn(_this, _ret);
    }

    _createClass(_class, [{
        key: 'onLaunch',
        value: function onLaunch() {
            var self = this;
        }
    }, {
        key: 'registerOrLogin',
        value: function registerOrLogin(cb) {
            var self = this;

            this.getUserInfo(function (userInfo) {
                wx.setStorage({
                    'key': 'self_user_info',
                    'data': userInfo,
                    'complete': function complete() {
                        // get current location coordinates
                        self.getLocationCoordinates(function (locationCoordinates) {
                            if (locationCoordinates == null) {
                                userInfo.longitude = 0;
                                userInfo.latitude = 0;
                            }
                            wx.getStorage({
                                'key': 'wx_token',
                                'success': function success(result) {
                                    console.log(result);
                                    sendRegisterOrLogonRequest(result.data, cb);
                                },
                                'fail': function fail(err) {
                                    console.log(err);
                                    sendRegisterOrLogonRequest(null, cb);
                                }
                            });
                            // send register / logon request, base on the wx_token retrieved from local storage
                            function sendRegisterOrLogonRequest(wx_token, cb) {
                                var requestData = {
                                    'wx_token': wx_token,
                                    'userInfo': userInfo
                                };
                                utils.sendAPIRequest({
                                    'apiName': 'register_login',
                                    'requestContentType': 'application/json',
                                    'requestData': requestData
                                }, function (res) {
                                    var success = res.data.success;
                                    var selfUserInfo = res.data.data;
                                    if (success) {
                                        console.log(selfUserInfo);
                                        selfUserInfo = utils.normalizeUserInfo(selfUserInfo);
                                        self.globalData.selfUserInfo = selfUserInfo;
                                        // connect to websocket
                                        self.socket = new Socket.default(constants.getWebSocketUrl(constants.getEnv()));
                                        wx.getStorage({
                                            'key': 'wx_token',
                                            'fail': function fail(err) {
                                                wx.setStorage({
                                                    'key': 'wx_token',
                                                    'data': selfUserInfo.wx_token
                                                });
                                            }
                                        });
                                        cb && cb(true);
                                    } else {
                                        cb && cb(false);
                                    }
                                }, function (err) {
                                    console.log(err);
                                    cb && cb(false);
                                }, null);
                            }
                        });
                    }
                });
            });
        }
    }, {
        key: 'getLocationCoordinates',
        value: function getLocationCoordinates(cb) {
            var self = this;

            _wepy2.default.getLocation({
                'success': function success(res) {
                    console.log('location: ' + res);
                    cb && cb(res);
                },
                'fail': function fail(err) {
                    console.log(err);
                    cb && cb(null);
                }
            });
        }
    }, {
        key: 'getUserInfo',
        value: function getUserInfo(cb) {
            var self = this;

            if (this.globalData.userInfo) {
                cb && cb(this.globalData.userInfo);
            }
            _wepy2.default.getUserInfo({
                'success': function success(res) {
                    self.globalData.userInfo = res.userInfo;
                    cb && cb(res.userInfo);
                },
                'fail': function fail(err) {
                    console.log(err);
                    cb && cb(null);
                }
            });
        }
    }, {
        key: 'getSelfUserInfo',
        value: function getSelfUserInfo(cb) {
            var self = this;

            if (this.globalData.selfUserInfo) {
                cb && cb(this.globalData.selfUserInfo);
            } else {
                wx.getStorage({
                    'key': 'wx_token',
                    'success': function success(result) {
                        var requestData = {};
                        utils.sendAPIRequest({
                            'apiName': 'get_user',
                            'queryString': '?wx_token=' + result.data,
                            'requestContentType': 'application/json',
                            'requestData': requestData
                        }, function (res) {
                            var success = res.data.success;
                            var selfUserInfo = res.data.data;
                            if (success) {
                                console.log(selfUserInfo);
                                selfUserInfo = utils.normalizeUserInfo(selfUserInfo);
                                self.globalData.selfUserInfo = selfUserInfo;
                                cb && cb(selfUserInfo);
                            }
                        }, function (err) {
                            console.log(err);
                            cb && cb(null);
                        }, null);
                    }
                });
            }
        }
    }, {
        key: 'setSelfUserInfo',
        value: function setSelfUserInfo(userInfo) {
            var self = this;

            this.globalData.selfUserInfo = userInfo;
        }
    }, {
        key: 'setSelfUserInfoField',
        value: function setSelfUserInfoField(fieldKey, fieldVal) {
            var self = this;

            this.globalData.selfUserInfo[fieldKey] = fieldVal;
        }
    }, {
        key: 'setSelfUserInfoProfileField',
        value: function setSelfUserInfoProfileField(fieldKey, fieldVal) {
            var self = this;

            this.globalData.selfUserInfo.profile[fieldKey] = fieldVal;
        }
    }, {
        key: 'updateConversationUserInfo',
        value: function updateConversationUserInfo(cb) {
            var self = this;

            wx.getStorage({
                'key': 'chat_conversations',
                'success': function success(result) {
                    // update conversation user name and images
                    var chatConversations = result.data;
                    var user_ids = Object.keys(chatConversations);
                    utils.sendAPIRequest({
                        'apiName': 'get_users_by_user_ids',
                        'queryString': '?wx_token=' + self.globalData.selfUserInfo.wx_token,
                        'requestContentType': 'application/json',
                        'requestData': {
                            'user_ids': user_ids
                        }
                    }, function (res) {
                        var users = res.data.data;
                        if (users.length > 0) {
                            for (var userInfo in users) {
                                var user_id = userInfo.email;
                                if (chatConversations[user_id] != null) {
                                    chatConversations[user_id].user_info = utils.normalizeUserInfo(userInfo);
                                }
                            }
                            wx.setStorage({
                                'key': 'chat_conversations',
                                'data': chatConversations,
                                'success': function success() {
                                    cb && cb();
                                }
                            });
                        } else {
                            cb && cb();
                        }
                    });
                },
                'fail': function fail(err) {
                    wx.setStorage({
                        'key': 'chat_conversations',
                        'data': {},
                        'success': function success() {
                            cb && cb();
                        }
                    });
                }
            });
        }
    }]);

    return _class;
}(_wepy2.default.app);


App(require('./npm/wepy/lib/wepy.js').default.$createApp(_class));
