var moment = require('moment');

var constants = require('./constants');

// Normalize Display Name
var normalizeDisplayName = function(userInfo) {
    return userInfo.profile.full_name != null && userInfo.profile.full_name != '' || userInfo.profile.nickname != null && userInfo.profile.nickname != '' ? userInfo.profile.full_name != null && userInfo.profile.full_name != '' ? userInfo.profile.full_name : userInfo.profile.nickname : '无名氏';
};
module.exports.normalizeDisplayName = normalizeDisplayName;

// Normalize Display Image Urls
var normalizeDisplayImageUrls = function(imageUrlArray) {
    var normalizedImageUrlArray = [];
    for (let i = 0; i < imageUrlArray.length; i++) {
        if (imageUrlArray[i].indexOf('://') === -1) {
            normalizedImageUrlArray[i] = constants.getUploadDirectoryUrl(constants.getEnv(), 'profile_images') + imageUrlArray[i];
        } else {
            normalizedImageUrlArray[i] = imageUrlArray[i];
        }
    }
    return normalizedImageUrlArray;
};
module.exports.normalizeDisplayImageUrls = normalizeDisplayImageUrls;

// Normalize User Info
var normalizeUserInfo = function(userInfo) {
    var userInfoFields = deepCopy(constants.getUserInfoFields());

    var birthday = null;
    if (userInfo.profile.birthday != null) {
        birthday = moment(userInfo.profile.birthday);
    }

    var basicInfo = userInfoFields.basic_info_fields;
    basicInfo.full_name.text = userInfo.profile.full_name;
    basicInfo.gender.val = userInfo.profile.gender;
    basicInfo.gender.text = userInfo.profile.gender == null ? null : constants.resolveGender(userInfo.profile.gender, 'cn');
    basicInfo.birthday.momentDate = birthday;
    basicInfo.birthday.text = userInfo.profile.birthday == null ? null : getReadableDateTimeString(birthday, true);
    basicInfo.age.text = userInfo.profile.birthday == null ? null : Math.floor(moment.duration(moment(new Date()).diff(moment(userInfo.profile.birthday))).asYears());
    basicInfo.c_zodiac.val = userInfo.profile.c_zodiac == null ? null : userInfo.profile.c_zodiac;
    basicInfo.c_zodiac.text = userInfo.profile.c_zodiac == null ? null : constants.resolveChineseZodiac(userInfo.profile.c_zodiac, 'cn');
    basicInfo.zodiac.val = userInfo.profile.zodiac == null ? null : userInfo.profile.zodiac;
    basicInfo.zodiac.text = userInfo.profile.zodiac == null ? null : constants.resolveZodiac(userInfo.profile.zodiac, 'cn');
    basicInfo.height.val = userInfo.profile.height == null ? null : userInfo.profile.height;
    basicInfo.height.text = userInfo.profile.height == null ? null : constants.resolveHeight(userInfo.profile.height, 'cn');
    basicInfo.weight.val = userInfo.profile.weight == null ? null : userInfo.profile.weight;
    basicInfo.weight.text = userInfo.profile.weight == null ? null : constants.resolveWeight(userInfo.profile.weight, 'cn');
    basicInfo.martial_status.val = userInfo.profile.martial_status == null ? null : userInfo.profile.martial_status;
    basicInfo.martial_status.text = userInfo.profile.martial_status == null ? null : constants.resolveMartialStatus(userInfo.profile.martial_status, 'cn');
    basicInfo.motto.text = userInfo.profile.motto;

    var detailedInfo = deepCopy(userInfoFields.detailed_info_fields);
    detailedInfo.personality.val = userInfo.profile.personality == null ? null : userInfo.profile.personality;
    detailedInfo.personality.text = userInfo.profile.personality == null ? null : constants.resolvePersonality(userInfo.profile.personality, 'cn');
    detailedInfo.interest.val_array = userInfo.profile.interest == null || userInfo.profile.interest.length === 0 ? null : deepCopy(userInfo.profile.interest);
    detailedInfo.interest.text = userInfo.profile.interest == null || userInfo.profile.interest.length === 0 ? null : constants.resolveInterest(userInfo.profile.interest, 'cn');
    detailedInfo.body_shape.val = userInfo.profile.body_shape == null ? null : userInfo.profile.body_shape;
    detailedInfo.body_shape.text = userInfo.profile.body_shape == null ? null : constants.resolveBodyShape(userInfo.profile.body_shape, 'cn');
    detailedInfo.residence_place.text = userInfo.profile.residence_place == null ? null : constants.resolveResidencePlace(userInfo.profile.residence_place);
    detailedInfo.birth_place.text = userInfo.profile.birth_place == null ? null : constants.resolveBirthPlace(userInfo.profile.birth_place);
    detailedInfo.education_level.val = userInfo.profile.education_level == null ? null : userInfo.profile.education_level;
    detailedInfo.education_level.text = userInfo.profile.education_level == null ? null : constants.resolveEducationLevel(userInfo.profile.education_level, 'cn');
    detailedInfo.undergrad_edu_institutions.text = userInfo.profile.undergrad_edu_institutions == null || userInfo.profile.undergrad_edu_institutions === '' ? null : userInfo.profile.undergrad_edu_institutions;
    detailedInfo.postgrad_edu_institutions.text = userInfo.profile.postgrad_edu_institutions == null || userInfo.profile.postgrad_edu_institutions === '' ? null : userInfo.profile.postgrad_edu_institutions;
    detailedInfo.occupation.val = userInfo.profile.occupation == null ? null : userInfo.profile.occupation;
    detailedInfo.occupation.text = userInfo.profile.occupation == null ? null : constants.resolveOccupation(userInfo.profile.occupation, 'cn');
    detailedInfo.employer.text = userInfo.profile.employer == null || userInfo.profile.employer === '' ? null : userInfo.profile.employer;
    detailedInfo.smoking.val = userInfo.profile.smoking == null ? null : userInfo.profile.smoking;
    detailedInfo.smoking.text = userInfo.profile.smoking == null ? null : constants.resolveSmoking(userInfo.profile.smoking, 'cn');
    detailedInfo.drinking.val = userInfo.profile.drinking == null ? null : userInfo.profile.drinking;
    detailedInfo.drinking.text = userInfo.profile.drinking == null ? null : constants.resolveDrinking(userInfo.profile.drinking, 'cn');
    detailedInfo.residential_status.val = userInfo.profile.residential_status == null ? null : userInfo.profile.residential_status;
    detailedInfo.residential_status.text = userInfo.profile.residential_status == null ? null : constants.resolveResidentialStatus(userInfo.profile.residential_status, 'cn');
    detailedInfo.language.val_array = userInfo.profile.language == null || userInfo.profile.language.length === 0 ? null : deepCopy(userInfo.profile.language);
    detailedInfo.language.text = userInfo.profile.language == null || userInfo.profile.language.length === 0 ? null : constants.resolveLanguage(userInfo.profile.language, 'cn');
    detailedInfo.religion.val = userInfo.profile.religion == null || userInfo.profile.religion.length === 0 ? null : userInfo.profile.religion;
    detailedInfo.religion.text = userInfo.profile.religion == null || userInfo.profile.religion.length === 0 ? null : constants.resolveReligion(userInfo.profile.religion, 'cn');
    detailedInfo.race.val = userInfo.profile.race == null ? null : userInfo.profile.race;
    detailedInfo.race.text = userInfo.profile.race == null ? null : constants.resolveRace(userInfo.profile.race, 'cn');
    detailedInfo.income.val = userInfo.profile.income == null ? null : userInfo.profile.income;
    detailedInfo.income.text = userInfo.profile.income == null ? null : constants.resolveIncome(userInfo.profile.income, 'cn');

    var moreInfo = deepCopy(userInfoFields.more_info_fields);
    moreInfo.about_me.text = userInfo.more_info == null ? null : userInfo.more_info.about_me;
    moreInfo.free_time_activities.text = userInfo.more_info == null ? null : userInfo.more_info.free_time_activities;
    moreInfo.favourite_things.text = userInfo.more_info == null ? null : userInfo.more_info.favourite_things;
    moreInfo.things_cannot_live_without.text = userInfo.more_info == null ? null : userInfo.more_info.things_cannot_live_without;
    moreInfo.adjectives_friends_describe_me.text = userInfo.more_info == null ? null : userInfo.more_info.adjectives_friends_describe_me;
    moreInfo.peoples_first_impression.text = userInfo.more_info == null ? null : userInfo.more_info.peoples_first_impression;
    moreInfo.things_people_do_not_notice_at_first.text = userInfo.more_info == null ? null : userInfo.more_info.things_people_do_not_notice_at_first;
    moreInfo.craziest_secret_silliest_things.text = userInfo.more_info == null ? null : userInfo.more_info.craziest_secret_silliest_things;
    moreInfo.most_influential_person.text = userInfo.more_info == null ? null : userInfo.more_info.most_influential_person;
    moreInfo.partner_qualities_looking_for.text = userInfo.more_info == null ? null : userInfo.more_info.partner_qualities_looking_for;
    moreInfo.question_to_matched_person.text = userInfo.more_info == null ? null : userInfo.more_info.question_to_matched_person;

    userInfo['basicInfo'] = basicInfo;
    userInfo['detailedInfo'] = detailedInfo;
    userInfo['moreInfo'] = moreInfo;

    userInfo['normalized_display_name'] = normalizeDisplayName(userInfo);
    userInfo['normalized_display_image_urls'] = normalizeDisplayImageUrls(userInfo.display_image_urls);
    if (userInfo['last_activity'] != null) {
        userInfo['minutes_since_last_activity'] = (getOffsetSeconds(userInfo['last_activity']) / 60).toFixed(0);
    }
    if (userInfo['distance'] != null) {
        userInfo['normalized_distance'] = (userInfo['distance'] / 1000).toFixed(2);
    }
    
    return userInfo;
};
module.exports.normalizeUserInfo = normalizeUserInfo;

// Fill Date Left With 0 If Single Digit
var fillLeftWithZero = function(num) {
    var str = num.toString();
    var pad = '00';
    return pad.substring(0, pad.length - str.length) + str;
};
module.exports.fillLeftWithZero = fillLeftWithZero;

// Get Offset Days
var getOffsetSeconds = function(sinceDateString) {
    return moment.duration(moment().subtract(moment(sinceDateString, 'YYYY-MM-DDTHH:mm:ss.SSSZ').toObject()).toObject()).asSeconds();
};
module.exports.getOffsetSeconds = getOffsetSeconds;

// Get Readable Date or Date Time String
var getReadableDateTimeString = function(date, withoutTime) {
    var mnt = moment(date);
    var str = mnt.year() + '-' + fillLeftWithZero(mnt.month() + 1) + '-' + fillLeftWithZero(mnt.date());
    if (withoutTime == null || !withoutTime) {
        str += ' ' + fillLeftWithZero(mnt.hour()) + ':' + fillLeftWithZero(mnt.minute()) + ':' + fillLeftWithZero(mnt.second());
    }
    return str;
};
module.exports.getReadableDateTimeString = getReadableDateTimeString;

// Resolve Touch Swipe Direction
var resolveSwipeDirection = function(touchStart, touchEnd) {
    // determine direction action
    var horizontalDistance = touchStart.pageX - touchEnd.pageX;
    var absHorizontalDistance = Math.abs(horizontalDistance);
    var verticalDistance = touchStart.pageY - touchEnd.pageY;
    var absVerticalDistance = Math.abs(verticalDistance);
    var action = '';
    if (absHorizontalDistance > 20 && absHorizontalDistance > absVerticalDistance) {
        if (horizontalDistance < 0) {
            console.log('direction: move right');
            action = 'moveRight';
        } else {
            console.log('direction: move left');
            action = 'moveLeft';
        }
    } else if (absVerticalDistance > 20 && absVerticalDistance > absHorizontalDistance) {
        if (verticalDistance < 0) {
            console.log('direction: move down');
            action = 'moveDown';
        } else {
            console.log('direction: move up');
            action = 'moveUp';
        }
    } else {}

    return action;
};
module.exports.resolveSwipeDirection = resolveSwipeDirection;

// Deep Copy
var deepCopy = function(obj) {
  if (Object.prototype.toString.call(obj) === '[object Array]') {
    var out = [];
    var len = obj.length;
    for (var i = 0; i < len; i++) {
      out[i] = deepCopy(obj[i]);
    }
    return out;
  }
  if (typeof obj === 'object') {
    var out = {};
    for (var i in obj) {
      out[i] = deepCopy(obj[i]);
    }
    return out;
  }
  return obj;
};
module.exports.deepCopy = deepCopy;

// Upload File
var uploadAndUpdateUserDisplayImages = function($page, apiUrl, displayImageUrls, callback) {
    wx.showToast({
        'icon': "loading...",
        'title': "正在上传..."
    });
    uploadSingleImageFile(0);
    function uploadSingleImageFile(displayImageUrlStartIndex) {
        var uploadDisplayImageTempFile = null;
        var noMoreImages = true;
        while (displayImageUrlStartIndex < displayImageUrls.length) {
            if (displayImageUrls[displayImageUrlStartIndex].startsWith('wxfile://')) {
                uploadDisplayImageTempFile = displayImageUrls[displayImageUrlStartIndex];
                break;
            }
            displayImageUrlStartIndex++;
        }
        var checkForMoreImageStartIndex = displayImageUrlStartIndex + 1;
        while (checkForMoreImageStartIndex < displayImageUrls.length) {
            if (displayImageUrls[checkForMoreImageStartIndex].startsWith('wxfile://')) {
                noMoreImages = false;
                break;
            }
            checkForMoreImageStartIndex++;
        }
        wx.uploadFile({
            'url': apiUrl,
            'filePath': uploadDisplayImageTempFile,
            'name': 'file',
            'header': { "Content-Type": "multipart/form-data" },
            'formData': {
                'display_image_urls': JSON.stringify(displayImageUrls),
                'image_index': displayImageUrlStartIndex,
                'no_more_images': noMoreImages.toString()
            },
            'success': function (res) {
                console.log(res);
                if (res.statusCode != 200) { 
                    wx.showModal({
                        'title': '提示',
                        'content': '上传失败',
                        'showCancel': false
                    });
                    callback('Server error, upload image failed.', null);
                }
                displayImageUrls[displayImageUrlStartIndex] = JSON.parse(res.data).data.uploaded_image_relative_path;
                if (noMoreImages) {
                    callback(null, displayImageUrls);
                } else {
                    uploadSingleImageFile(displayImageUrlStartIndex + 1);
                }
            },
            'fail': function (err) {
                console.log(err);
                wx.showModal({
                    'title': '提示',
                    'content': '上传失败',
                    'showCancel': false
                });
                callback(err, null);
            }
        });
    }
};
module.exports.uploadAndUpdateUserDisplayImages = uploadAndUpdateUserDisplayImages;

// Build Request Query String
var buildRequestQueryString = function(obj) {
    var queryString = '?';
    for (let key in obj) {
        queryString += key + '=' + obj[key] + '&';
    }
    return queryString;
};
module.exports.buildRequestQueryString = buildRequestQueryString;

// Send API Request
/*
    options: {
        'apiName': String,
        'queryString': String | null | <optional>
        'requestMethod': String,
        'requestContentType': String,
        'requestData': Object | null | <optional>
    }
*/
var sendAPIRequest = function(options, successCallback, failCallback, completeCallback) {
    wx.request({
        'url': constants.getApiUrl(constants.getEnv(), options.apiName) + (options.queryString == null ? '' : options.queryString),
        'method': constants.getApiAction(options.apiName),
        'header': {
            'content-type': options.requestContentType
        },
        'data': options.requestData == null ? {} : options.requestData,
        'success': function(res) {
            successCallback && successCallback(res);
        },
        'fail': function(err) {
            failCallback && failCallback(err);
        },
        'complete': function() {
            completeCallback && completeCallback();
        }
    });
};
module.exports.sendAPIRequest = sendAPIRequest;