var Religion = function() {
    this.data_mapping = {
        "-1": {
            "en": "Not Specified",
            "cn": "未填"
        },
        "1": {
            "en": "Nonreligious",
            "cn": "无"
        },
        "2": {
            "en": "Atheism",
            "cn": "无神论"
        },
        "3": {
            "en": "Buddhism",
            "cn": "佛教"
        },
        "4": {
            "en": "Christians",
            "cn": "基督教"
        },
        "5": {
            "en": "Muslims",
            "cn": "穆斯林"
        },
        "6": {
            "en": "Hinduism",
            "cn": "印度教"
        },
        "7": {
            "en": "Islam",
            "cn": "伊斯兰教"
        },
        "8": {
            "en": "Judaism",
            "cn": "犹太教"
        },
        "9": {
            "en": "Shinto",
            "cn": "神道教"
        },
        "999": {
            "en": "Other",
            "cn": "其它"
        }
    };
};

Religion.prototype.getValueString = function(key, lang) {
    return this.data_mapping[key] == null ? null : this.data_mapping[key][lang];
};

module.exports = Religion;