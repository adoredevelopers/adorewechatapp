var Drinking = function() {
    this.data_mapping = {
        "-1": {
            "en": "Not Specified",
            "cn": "未填"
        },
        "1": {
            "en": "No",
            "cn": "滴酒不沾"
        },
        "2": {
            "en": "Ocassional",
            "cn": "偶尔喝喝"
        },
        "3": {
            "en": "Casual Drinker",
            "cn": "社交聚会"
        },
        "4": {
            "en": "Moderate",
            "cn": "半斤八两"
        },
        "5": {
            "en": "A Way Of Life",
            "cn": "千杯不倒"
        }
    };
};

Drinking.prototype.getValueString = function(key, lang) {
    return this.data_mapping[key] == null ? null : this.data_mapping[key][lang];
};

module.exports = Drinking;