var Race = function() {
    this.data_mapping = {
        "-1": {
            "en": "Not Specified",
            "cn": "未填"
        },
        "1": {
            "en": "Chinese / Ethnic Chinese",
            "cn": "华人 / 华裔"
        },
        "2": {
            "en": "Cambodian",
            "cn": "柬埔寨"
        },
        "3": {
            "en": "Filipino",
            "cn": "菲律宾"
        },
        "4": {
            "en": "Japanese",
            "cn": "日本"
        },
        "5": {
            "en": "Korean",
            "cn": "韩国"
        },
        "6": {
            "en": "Laotian",
            "cn": "老挝"
        },
        "7": {
            "en": "Malaysian",
            "cn": "马来西亚"
        },
        "8": {
            "en": "Thai",
            "cn": "泰国"
        },
        "9": {
            "en": "Vietnamese",
            "cn": "越南"
        },
        "10": {
            "en": "Asian",
            "cn": "亚裔"
        },
        "11": {
            "en": "Korean",
            "cn": "韩国"
        },
        "12": {
            "en": "Black",
            "cn": "黑人"
        },
        "13": {
            "en": "Hispanic / Latin",
            "cn": "西班牙 / 拉丁"
        },
        "14": {
            "en": "Indian",
            "cn": "印度"
        },
        "15": {
            "en": "Middle Eastern",
            "cn": "中东"
        },
        "16": {
            "en": "Native American",
            "cn": "北美土著"
        },
        "17": {
            "en": "White / Caucasian",
            "cn": "白人 / 高加索人"
        },
        "18": {
            "en": "Mixed",
            "cn": "混血"
        },
        "19": {
            "en": "Other",
            "cn": "其他"
        },
        "100": {
            "en": "Han",
            "cn": "汉族"
        },
        "101": {
            "en": "Mongol",
            "cn": "蒙古族"
        },
        "102": {
            "en": "Hui",
            "cn": "回族"
        },
        "103": {
            "en": "Tibetan",
            "cn": "藏族"
        },
        "104": {
            "en": "Uyghur",
            "cn": "维吾尔族"
        },
        "105": {
            "en": "Miao",
            "cn": "苗族"
        },
        "106": {
            "en": "Yi",
            "cn": "彝族"
        },
        "107": {
            "en": "Zhuang",
            "cn": "壮族"
        },
        "108": {
            "en": "Buyei",
            "cn": "布依族"
        },
        "109": {
            "en": "Chosen",
            "cn": "朝鲜族"
        },
        "110": {
            "en": "Man",
            "cn": "满族"
        },
        "111": {
            "en": "Dong",
            "cn": "侗族"
        },
        "112": {
            "en": "Yao",
            "cn": "瑶族"
        },
        "113": {
            "en": "Bai",
            "cn": "白族"
        },
        "114": {
            "en": "Tujia",
            "cn": "土家族"
        },
        "115": {
            "en": "Hani",
            "cn": "哈尼族"
        },
        "116": {
            "en": "Hazak",
            "cn": "哈萨克族"
        },
        "117": {
            "en": "Dai",
            "cn": "傣族"
        },
        "118": {
            "en": "Li",
            "cn": "黎族"
        },
        "119": {
            "en": "Lisu",
            "cn": "傈僳族"
        },
        "120": {
            "en": "Va",
            "cn": "佤族"
        },
        "121": {
            "en": "She",
            "cn": "畲族"
        },
        "122": {
            "en": "Lahu",
            "cn": "拉祜族"
        },
        "123": {
            "en": "Shui",
            "cn": "水族"
        },
        "124": {
            "en": "Dongxiang",
            "cn": "东乡族"
        },
        "125": {
            "en": "Naxi",
            "cn": "纳西族"
        },
        "126": {
            "en": "Jingpo",
            "cn": "景颇族"
        },
        "127": {
            "en": "Kirgiz",
            "cn": "柯尔克孜族"
        },
        "128": {
            "en": "Tu",
            "cn": "土族"
        },
        "129": {
            "en": "Daur",
            "cn": "达斡尔族"
        },
        "130": {
            "en": "Mulao",
            "cn": "仫佬族"
        },
        "131": {
            "en": "Qiang",
            "cn": "羌族"
        },
        "132": {
            "en": "Blang",
            "cn": "布朗族"
        },
        "133": {
            "en": "Salar",
            "cn": "撒拉族"
        },
        "134": {
            "en": "Maonan",
            "cn": "毛南族"
        },
        "135": {
            "en": "Gelao",
            "cn": "仡佬族"
        },
        "136": {
            "en": "Xibe",
            "cn": "锡伯族"
        },
        "137": {
            "en": "Achang",
            "cn": "阿昌族"
        },
        "138": {
            "en": "Pumi",
            "cn": "普米族"
        },
        "139": {
            "en": "Tajik",
            "cn": "塔吉克族"
        },
        "140": {
            "en": "Nu",
            "cn": "怒族"
        },
        "141": {
            "en": "Uzbek",
            "cn": "乌兹别克族"
        },
        "142": {
            "en": "Russ",
            "cn": "俄罗斯族"
        },
        "143": {
            "en": "Ewenki",
            "cn": "鄂温克族"
        },
        "144": {
            "en": "Deang",
            "cn": "德昂族"
        },
        "145": {
            "en": "Bonan",
            "cn": "保安族"
        },
        "146": {
            "en": "Yugur",
            "cn": "裕固族"
        },
        "147": {
            "en": "Gin",
            "cn": "京族"
        },
        "148": {
            "en": "Tatar",
            "cn": "塔塔尔族"
        },
        "149": {
            "en": "Derung",
            "cn": "独龙族"
        },
        "150": {
            "en": "Oroqen",
            "cn": "鄂伦春族"
        },
        "151": {
            "en": "Hezhen",
            "cn": "赫哲族"
        },
        "152": {
            "en": "Monba",
            "cn": "门巴族"
        },
        "153": {
            "en": "Lhoba",
            "cn": "珞巴族"
        },
        "154": {
            "en": "Jino",
            "cn": "基诺族"
        },
        "155": {
            "en": "Gaoshan",
            "cn": "高山族"
        },
    };
};

Race.prototype.getValueString = function(key, lang) {
    return this.data_mapping[key] == null ? null : this.data_mapping[key][lang];
};

module.exports = Race;