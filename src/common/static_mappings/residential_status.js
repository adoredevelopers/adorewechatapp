var ResidentialStatus = function() {
    this.data_mapping = {
        "-1": {
            "en": "Not Specified",
            "cn": "未填"
        },
        "1": {
            "en": "Citizen",
            "cn": "公民"
        },
        "2": {
            "en": "Green Card",
            "cn": "绿卡"
        },
        "3": {
            "en": "Permanent resident",
            "cn": "永久居民"
        },
        "4": {
            "en": "Study Visa",
            "cn": "学习签证"
        },
        "5": {
            "en": "Work Visa",
            "cn": "工作签证"
        },
        "6": {
            "en": "Other",
            "cn": "其它"
        }
    };
};

ResidentialStatus.prototype.getValueString = function(key, lang) {
    return this.data_mapping[key] == null ? null : this.data_mapping[key][lang];
};

module.exports = ResidentialStatus;