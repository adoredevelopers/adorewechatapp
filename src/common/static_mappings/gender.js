var Gender = function() {
    this.data_mapping = {
        'Female': {
            'en': 'Female',
            'cn': '女'
        },
        'Male': {
            'en': 'Male',
            'cn': '男'
        }
    };
};

Gender.prototype.getValueString = function(key, lang) {
    return this.data_mapping[key] == null ? null : this.data_mapping[key][lang];
};

module.exports = Gender;