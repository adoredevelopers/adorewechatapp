var Language = function() {
    this.data_mapping = {
        "1": {
            "en":"English",
            "cn":"英文"
        },
        "2": {
            "en":"Mandarin",
            "cn":"国语"
        },
        "3": {
            "en":"Cantonese",
            "cn":"粤语（繁体字）"
        },
        "4": {
            "en":"French",
            "cn":"法语"
        },
        "5": {
            "en":"Afrikaans",
            "cn":"南非洲的荷兰语"
        },
        "6": {
            "en":"Albanian",
            "cn":"阿尔巴尼亚语"
        },
        "7": {
            "en":"Arabic",
            "cn":"阿拉伯语"
        },
        "8": {
            "en":"Armenian",
            "cn":"亚美尼亚语"
        },
        "9": {
            "en":"Basque",
            "cn":"巴斯克语"
        },
        "10": {
            "en":"Bengali",
            "cn":"孟加拉语"
        },
        "11": {
            "en":"Bulgarian",
            "cn":"保加利亚语"
        },
        "12": {
            "en":"Catalan",
            "cn":"加泰罗尼亚语"
        },
        "13": {
            "en":"Cambodian",
            "cn":"柬埔寨语"
        },
        "14": {
            "en":"Croatian",
            "cn":"克罗地亚人语"
        },
        "15": {
            "en":"Czech",
            "cn":"捷克语"
        },
        "16": {
            "en":"Danish",
            "cn":"丹麦语"
        },
        "17": {
            "en":"Dutch",
            "cn":"荷兰语"
        },
        "18": {
            "en":"Estonian",
            "cn":"爱沙尼亚语"
        },
        "19": {
            "en":"Fiji",
            "cn":"斐济语"
        },
        "20": {
            "en":"Finnish",
            "cn":"芬兰语"
        },
        "21": {
            "en":"Georgian",
            "cn":"乔治亚语"
        },
        "22": {
            "en":"German",
            "cn":"德语"
        },
        "23": {
            "en":"Greek",
            "cn":"希腊语"
        },
        "24": {
            "en":"Gujarati",
            "cn":"古吉拉特语"
        },
        "25": {
            "en":"Hebrew",
            "cn":"希伯来语"
        },
        "26": {
            "en":"Hindi",
            "cn":"北印度语"
        },
        "27": {
            "en":"Hungarian",
            "cn":"匈牙利语"
        },
        "28": {
            "en":"Icelandic",
            "cn":"冰岛语"
        },
        "29": {
            "en":"Indonesian",
            "cn":"印尼语群"
        },
        "30": {
            "en":"Irish",
            "cn":"爱尔兰语"
        },
        "31": {
            "en":"Italian",
            "cn":"意大利语"
        },
        "32": {
            "en":"Japanese",
            "cn":"日语"
        },
        "33": {
            "en":"Javanese",
            "cn":"爪哇语"
        },
        "34": {
            "en":"Korean",
            "cn":"韩语"
        },
        "35": {
            "en":"Latin",
            "cn":"拉丁语"
        },
        "36": {
            "en":"Latvian",
            "cn":"拉脱维亚语"
        },
        "37": {
            "en":"Lithuanian",
            "cn":"立陶宛语"
        },
        "38": {
            "en":"Macedonian",
            "cn":"马其顿语"
        },
        "39": {
            "en":"Malay",
            "cn":"马来语"
        },
        "40": {
            "en":"Malayalam",
            "cn":"印度西南部语"
        },
        "41": {
            "en":"Maltese",
            "cn":"马尔他语"
        },
        "42": {
            "en":"Maori",
            "cn":"毛利语"
        },
        "43": {
            "en":"Marathi",
            "cn":"马拉地语"
        },
        "44": {
            "en":"Mongolian",
            "cn":"蒙古语"
        },
        "45": {
            "en":"Nepali",
            "cn":"尼泊尔语"
        },
        "46": {
            "en":"Norwegian",
            "cn":"挪威语"
        },
        "47": {
            "en":"Persian",
            "cn":"波斯语"
        },
        "48": {
            "en":"Polish",
            "cn":"波兰语"
        },
        "49": {
            "en":"Portuguese",
            "cn":"葡萄牙语"
        },
        "50": {
            "en":"Punjabi",
            "cn":"旁遮普语"
        },
        "51": {
            "en":"Quechua",
            "cn":"盖丘亚语"
        },
        "52": {
            "en":"Romanian",
            "cn":"罗马尼亚语"
        },
        "53": {
            "en":"Russian",
            "cn":"俄语"
        },
        "54": {
            "en":"Samoan",
            "cn":"萨摩亚语"
        },
        "55": {
            "en":"Serbian",
            "cn":"塞尔维亚语"
        },
        "56": {
            "en":"Slovak",
            "cn":"斯洛伐克人语"
        },
        "57": {
            "en":"Slovenian",
            "cn":"斯洛文尼亚语"
        },
        "58": {
            "en":"Spanish",
            "cn":"西班牙语"
        },
        "59": {
            "en":"Swahili",
            "cn":"斯瓦希里语"
        },
        "60": {
            "en":"Swedish",
            "cn":"瑞典语"
        },
        "61": {
            "en":"Tamil",
            "cn":"坦米尔语"
        },
        "62": {
            "en":"Tatar",
            "cn":"鞑靼语"
        },
        "63": {
            "en":"Telugu",
            "cn":"泰卢固语"
        },
        "64": {
            "en":"Thai",
            "cn":"泰国语"
        },
        "65": {
            "en":"Tibetan",
            "cn":"藏语"
        },
        "66": {
            "en":"Tonga",
            "cn":"汤加语"
        },
        "67": {
            "en":"Turkish",
            "cn":"土耳其语"
        },
        "68": {
            "en":"Ukrainian",
            "cn":"乌克兰语"
        },
        "69": {
            "en":"Urdu",
            "cn":"乌尔都语"
        },
        "70": {
            "en":"Uzbek",
            "cn":"乌兹别克语"
        },
        "71": {
            "en":"Vietnamese",
            "cn":"土耳其语"
        },
        "72": {
            "en":"Welsh",
            "cn":"威尔士语"
        },
        "73": {
            "en":"Xhosa",
            "cn":"班图语"
        },
        "999": {
            "en":"Other",
            "cn":"其它"
        }
    };
};

Language.prototype.getValueString = function(keyArray, lang) {
    var valueString = '';
    for (let i = 0; i < keyArray.length; i++) {
        var key = keyArray[i];
        if (this.data_mapping[key] != null) {
            valueString += this.data_mapping[key][lang] + ', ';
        }
    }
    if (valueString.length > 0) {
        valueString = valueString.substring(0, valueString.length - 2); // get rid of the last comma
    }
    return valueString;
};

module.exports = Language;