// Require Basic Info Static Mappings
var Gender = require('./static_mappings/gender');
module.exports.Gender = Gender;
var ChineseZodiac = require('./static_mappings/chinese_zodiac');
module.exports.ChineseZodiac = ChineseZodiac;
var Zodiac = require('./static_mappings/zodiac');
module.exports.Zodiac = Zodiac;
var Height = require('./static_mappings/height');
module.exports.Height = Height;
var Weight = require('./static_mappings/weight');
module.exports.Weight = Weight;
var MartialStatus = require('./static_mappings/martial_status');
module.exports.MartialStatus = MartialStatus;
// Require Detailed Info Static Mappings
var Personality = require('./static_mappings/personality');
module.exports.Personality = Personality;
var Interest = require('./static_mappings/interest');
module.exports.Interest = Interest;
var BodyShape = require('./static_mappings/body_shape');
module.exports.BodyShape = BodyShape;
var EducationLevel = require('./static_mappings/education_level');
module.exports.EducationLevel = EducationLevel;
var Occupation = require('./static_mappings/occupation');
module.exports.Occupation = Occupation;
var Smoking = require('./static_mappings/smoking');
module.exports.Smoking = Smoking;
var Drinking = require('./static_mappings/drinking');
module.exports.Drinking = Drinking;
var ResidentialStatus = require('./static_mappings/residential_status');
module.exports.ResidentialStatus = ResidentialStatus;
var Language = require('./static_mappings/language');
module.exports.Language = Language;
var Religion = require('./static_mappings/religion');
module.exports.Religion = Religion;
var Race = require('./static_mappings/race');
module.exports.Race = Race;
var Income = require('./static_mappings/income');
module.exports.Income = Income;
// Require Other Static Mappings
var Activity = require('./static_mappings/activity');
module.exports.Activity = Activity;



// Constants
var constants = {
    'env': 'production',
    'development': {
        'api_url_base':'http://localhost:15522',
        'upload_dir_url_base': 'http://localhost:15522/uploads',
        'websocket_url': 'ws://localhost:15522'
    },
    'production': {
        'api_url_base':'http://api.adoreapp.ca',
        'upload_dir_url_base': 'http://api.adoreapp.ca/uploads',
        'websocket_url': 'ws://api.adoreapp.ca'
    },
    'common': {
        'api_names': {
            'register_login': {
                'api_path': '/wx_register_login',
                'api_action': 'POST'
            },
            'get_user': {
                'api_path': '/get_user',
                'api_action': 'GET'
            },
            'get_nearby_users': {
                'api_path': '/get_nearby_users',
                'api_action': 'GET'
            },
            'get_user_conversation': {
                'api_path': '/get_user_conversation',
                'api_action': 'GET'
            },
            'get_users_by_user_ids': {
                'api_path': '/get_users_by_user_ids',
                'api_action': 'POST'
            },
            'update_user_profile': {
                'api_path': '/update_user_profile',
                'api_action': 'POST'
            },
            'update_user_fields': {
                'api_path': '/update_user_fields',
                'api_action': 'POST'
            },
            'update_user_display_images': {
                'api_path': '/update_user_display_images',
                'api_action': 'POST'
            }
        },
        'dir_names': {
            'profile_images': '/profile_images/',
            'partner_images': '/partner_images/'
        },
        'account_types': {
            'basic': ['Normal'],
            'premium': ['Premium', 'Mod', 'Admin'],
            'mod': ['Mod', 'Admin'],
            'admin': ['Admin']
        },
        'nearby_user_fetch_constants': {
            'batch_size': 20,
            'allowed_paging_count_normal': 5,
            'allowed_paging_count_premium': 25
        },
        'user_info_fields': {
            'basic_info_fields': {
                'full_name': {
                    'sequence': 1,
                    'input_type': 'TEXTFIELD',
                    'hidden': {
                        'UserDetailsPage': false,
                        'MyProfilePage': false,
                        'EditProfilePage': false
                    },
                    'label': '姓名'
                },
                'gender': {
                    'sequence': 2,
                    'input_type': 'PICKER',
                    'hidden': {
                        'UserDetailsPage': false,
                        'MyProfilePage': false,
                        'EditProfilePage': false
                    },
                    'label': '性别'
                },
                'birthday': {
                    'sequence': 3,
                    'input_type': 'PICKER',
                    'hidden': {
                        'UserDetailsPage': true,
                        'MyProfilePage': true,
                        'EditProfilePage': false
                    },
                    'label': '生日'
                },
                'age': {
                    'sequence': -1,
                    'input_type': 'HIDDEN',
                    'hidden': {
                        'UserDetailsPage': false,
                        'MyProfilePage': false,
                        'EditProfilePage': true
                    },
                    'label': '年龄'
                },
                'c_zodiac': {
                    'sequence': 4,
                    'input_type': 'HIDDEN',
                    'hidden': {
                        'UserDetailsPage': false,
                        'MyProfilePage': false,
                        'EditProfilePage': true
                    },
                    'label': '生肖'
                },
                'zodiac': {
                    'sequence': 5,
                    'input_type': 'HIDDEN',
                    'hidden': {
                        'UserDetailsPage': false,
                        'MyProfilePage': false,
                        'EditProfilePage': true
                    },
                    'label': '星座'
                },
                'height': {
                    'sequence': 6,
                    'input_type': 'PICKER',
                    'hidden': {
                        'UserDetailsPage': false,
                        'MyProfilePage': false,
                        'EditProfilePage': false
                    },
                    'label': '身高'
                },
                'weight': {
                    'sequence': 7,
                    'input_type': 'PICKER',
                    'hidden': {
                        'UserDetailsPage': false,
                        'MyProfilePage': false,
                        'EditProfilePage': false
                    },
                    'label': '体重'
                },
                'martial_status': {
                    'sequence': 8,
                    'input_type': 'PICKER',
                    'hidden': {
                        'UserDetailsPage': false,
                        'MyProfilePage': false,
                        'EditProfilePage': false
                    },
                    'label': '婚姻状况'
                },
                'motto': {
                    'sequence': 9,
                    'input_type': 'TEXTFIELD',
                    'hidden': {
                        'UserDetailsPage': false,
                        'MyProfilePage': false,
                        'EditProfilePage': false
                    },
                    'label': '个性签名'
                }
            },
            'detailed_info_fields': {
                'personality': {
                    'sequence': 1,
                    'input_type': 'PICKER',
                    'hidden': {
                        'UserDetailsPage': false,
                        'MyProfilePage': false,
                        'EditProfilePage': false
                    },
                    'label': '性格'
                },
                'interest': {
                    'sequence': 2,
                    'input_type': 'MULTISELECT',
                    'expand_view_hidden_class': 'hidden',
                    'hidden': {
                        'UserDetailsPage': false,
                        'MyProfilePage': false,
                        'EditProfilePage': false
                    },
                    'label': '兴趣'
                },
                'body_shape': {
                    'sequence': 3,
                    'input_type': 'PICKER',
                    'hidden': {
                        'UserDetailsPage': false,
                        'MyProfilePage': false,
                        'EditProfilePage': false
                    },
                    'label': '身材'
                },
                'residence_place': {
                    'sequence': 4,
                    'input_type': 'TEXTFIELD',
                    'hidden': {
                        'UserDetailsPage': false,
                        'MyProfilePage': false,
                        'EditProfilePage': false
                    },
                    'label': '居住地'
                },
                'birth_place': {
                    'sequence': 5,
                    'input_type': 'TEXTFIELD',
                    'hidden': {
                        'UserDetailsPage': false,
                        'MyProfilePage': false,
                        'EditProfilePage': false
                    },
                    'label': '出生地'
                },
                'education_level': {
                    'sequence': 6,
                    'input_type': 'PICKER',
                    'hidden': {
                        'UserDetailsPage': false,
                        'MyProfilePage': false,
                        'EditProfilePage': false
                    },
                    'label': '最高学术教育程度'
                },
                'undergrad_edu_institutions': {
                    'sequence': 7,
                    'input_type': 'TEXTFIELD',
                    'hidden': {
                        'UserDetailsPage': false,
                        'MyProfilePage': false,
                        'EditProfilePage': false
                    },
                    'label': '本科院校'
                },
                'postgrad_edu_institutions': {
                    'sequence': 8,
                    'input_type': 'TEXTFIELD',
                    'hidden': {
                        'UserDetailsPage': false,
                        'MyProfilePage': false,
                        'EditProfilePage': false
                    },
                    'label': '研究生院校'
                },
                'occupation': {
                    'sequence': 9,
                    'input_type': 'PICKER',
                    'hidden': {
                        'UserDetailsPage': false,
                        'MyProfilePage': false,
                        'EditProfilePage': false
                    },
                    'label': '职业'
                },
                'employer': {
                    'sequence': 10,
                    'input_type': 'TEXTFIELD',
                    'hidden': {
                        'UserDetailsPage': false,
                        'MyProfilePage': false,
                        'EditProfilePage': false
                    },
                    'label': '受雇公司'
                },
                'smoking': {
                    'sequence': 11,
                    'input_type': 'PICKER',
                    'hidden': {
                        'UserDetailsPage': false,
                        'MyProfilePage': false,
                        'EditProfilePage': false
                    },
                    'label': '烟'
                },
                'drinking': {
                    'sequence': 12,
                    'input_type': 'PICKER',
                    'hidden': {
                        'UserDetailsPage': false,
                        'MyProfilePage': false,
                        'EditProfilePage': false
                    },
                    'label': '酒'
                },
                'residential_status': {
                    'sequence': 13,
                    'input_type': 'PICKER',
                    'hidden': {
                        'UserDetailsPage': false,
                        'MyProfilePage': false,
                        'EditProfilePage': false
                    },
                    'label': '居民身份'
                },
                'language': {
                    'sequence': 14,
                    'input_type': 'MULTISELECT',
                    'expand_view_hidden_class': 'hidden',
                    'hidden': {
                        'UserDetailsPage': false,
                        'MyProfilePage': false,
                        'EditProfilePage': false
                    },
                    'label': '语言'
                },
                'religion': {
                    'sequence': 15,
                    'input_type': 'PICKER',
                    'expand_view_hidden_class': 'hidden',
                    'hidden': {
                        'UserDetailsPage': false,
                        'MyProfilePage': false,
                        'EditProfilePage': false
                    },
                    'label': '宗教'
                },
                'race': {
                    'sequence': 16,
                    'input_type': 'PICKER',
                    'hidden': {
                        'UserDetailsPage': false,
                        'MyProfilePage': false,
                        'EditProfilePage': false
                    },
                    'label': '种族/民族'
                },
                'income': {
                    'sequence': 17,
                    'input_type': 'PICKER',
                    'hidden': {
                        'UserDetailsPage': false,
                        'MyProfilePage': false,
                        'EditProfilePage': false
                    },
                    'label': '收入'
                },
            },
            'more_info_fields': {
                'about_me': {
                    'sequence': 1,
                    'input_type': 'TEXTAREA',
                    'hidden': {
                        'UserDetailsPage': false,
                        'MyProfilePage': false,
                        'EditProfilePage': false
                    },
                    'label': '关于我'
                },
                'free_time_activities': {
                    'sequence': 2,
                    'input_type': 'TEXTAREA',
                    'hidden': {
                        'UserDetailsPage': false,
                        'MyProfilePage': false,
                        'EditProfilePage': false
                    },
                    'label': '空闲时间的安排'
                },
                'favourite_things': {
                    'sequence': 3,
                    'input_type': 'TEXTAREA',
                    'hidden': {
                        'UserDetailsPage': false,
                        'MyProfilePage': false,
                        'EditProfilePage': false
                    },
                    'label': '最喜欢的事物'
                },
                'things_cannot_live_without': {
                    'sequence': 4,
                    'input_type': 'TEXTAREA',
                    'hidden': {
                        'UserDetailsPage': false,
                        'MyProfilePage': false,
                        'EditProfilePage': false
                    },
                    'label': '生活中不可缺少的种种'
                },
                'adjectives_friends_describe_me': {
                    'sequence': 5,
                    'input_type': 'TEXTAREA',
                    'hidden': {
                        'UserDetailsPage': false,
                        'MyProfilePage': false,
                        'EditProfilePage': false
                    },
                    'label': '朋友会用这五个形容词来形容我'
                },
                'peoples_first_impression': {
                    'sequence': 6,
                    'input_type': 'TEXTAREA',
                    'hidden': {
                        'UserDetailsPage': false,
                        'MyProfilePage': false,
                        'EditProfilePage': false
                    },
                    'label': '陌生人对我的第一印象'
                },
                'things_people_do_not_notice_at_first': {
                    'sequence': 7,
                    'input_type': 'TEXTAREA',
                    'hidden': {
                        'UserDetailsPage': false,
                        'MyProfilePage': false,
                        'EditProfilePage': false
                    },
                    'label': '与我初识时不容易了解到的方面'
                },
                'craziest_secret_silliest_things': {
                    'sequence': 8,
                    'input_type': 'TEXTAREA',
                    'hidden': {
                        'UserDetailsPage': false,
                        'MyProfilePage': false,
                        'EditProfilePage': false
                    },
                    'label': '做过的最疯狂、秘密、傻呆萌的事情'
                },
                'most_influential_person': {
                    'sequence': 9,
                    'input_type': 'TEXTAREA',
                    'hidden': {
                        'UserDetailsPage': false,
                        'MyProfilePage': false,
                        'EditProfilePage': false
                    },
                    'label': '对我最有影响的人'
                },
                'partner_qualities_looking_for': {
                    'sequence': 10,
                    'input_type': 'TEXTAREA',
                    'hidden': {
                        'UserDetailsPage': false,
                        'MyProfilePage': false,
                        'EditProfilePage': false
                    },
                    'label': '我期待在另一半身上看到的品质'
                },
                'question_to_matched_person': {
                    'sequence': 11,
                    'input_type': 'TEXTAREA',
                    'hidden': {
                        'UserDetailsPage': false,
                        'MyProfilePage': false,
                        'EditProfilePage': false
                    },
                    'label': '给对我有兴趣的人提的一个问题'
                }
            }
        }
    }
};
module.exports.constants = constants;

var getEnv = function() {
    return constants.env;
};
module.exports.getEnv = getEnv;

var getApiUrl = function(env, apiName) {
    return constants[env].api_url_base + constants.common.api_names[apiName].api_path;
};
module.exports.getApiUrl = getApiUrl;

var getApiAction = function(apiName) {
    return constants.common.api_names[apiName].api_action;
};
module.exports.getApiAction = getApiAction;

var getUploadDirectoryUrl = function(env, dirName) {
    return constants[env].upload_dir_url_base + constants.common.dir_names[dirName];
};
module.exports.getUploadDirectoryUrl = getUploadDirectoryUrl;

var getWebSocketUrl = function(env) {
    return constants[env].websocket_url;
};
module.exports.getWebSocketUrl = getWebSocketUrl;

var getUserInfoFields = function() {
    return constants.common.user_info_fields;
};
module.exports.getUserInfoFields = getUserInfoFields;



/* Resolve Static Mappings */
// Resolve Basic Info
var resolveGender = function(key, lang) {
    var gender = new Gender();
    return gender.getValueString(key, lang);
};
module.exports.resolveGender = resolveGender;

var resolveChineseZodiac = function(key, lang) {
    var chineseZodiac = new ChineseZodiac();
    return chineseZodiac.getValueString(key, lang);
};
module.exports.resolveChineseZodiac = resolveChineseZodiac;

var resolveZodiac = function(key, lang) {
    var zodiac = new Zodiac();
    return zodiac.getValueString(key, lang);
};
module.exports.resolveZodiac = resolveZodiac;

var resolveHeight = function(key, lang) {
    var height = new Height();
    return height.getValueString(key, lang);
};
module.exports.resolveHeight = resolveHeight;

var resolveWeight = function(key, lang) {
    var weight = new Weight();
    return weight.getValueString(key, lang);
};
module.exports.resolveWeight = resolveWeight;

var resolveMartialStatus = function(key, lang) {
    var martialStatus = new MartialStatus();
    return martialStatus.getValueString(key, lang);
};
module.exports.resolveMartialStatus = resolveMartialStatus;



// Resolve Detailed Info
var resolvePersonality = function(keyArray, lang) {
    var personality = new Personality();
    return personality.getValueString(keyArray, lang);
};
module.exports.resolvePersonality = resolvePersonality;

var resolveInterest = function(keyArray, lang) {
    var interest = new Interest();
    return interest.getValueString(keyArray, lang);
};
module.exports.resolveInterest = resolveInterest;

var resolveBodyShape = function(key, lang) {
    var bodyShape = new BodyShape();
    return bodyShape.getValueString(key, lang);
};
module.exports.resolveBodyShape = resolveBodyShape;

var resolveEducationLevel = function(key, lang) {
    var educationLevel = new EducationLevel();
    return educationLevel.getValueString(key, lang);
};
module.exports.resolveEducationLevel = resolveEducationLevel;

var resolveBirthPlace = function(obj) {
    if (obj.birth_city && obj.birth_province && obj.birth_country) {
        return obj.birth_city + ', ' + obj.birth_province + ', ' + obj.birth_country;
    }
    return null;
};
module.exports.resolveBirthPlace = resolveBirthPlace;

var resolveResidencePlace = function(obj) {
    if (obj.residence_city && obj.residence_province && obj.residence_country) {
        return obj.residence_city + ', ' + obj.residence_province + ', ' + obj.residence_country;
    }
    return null;
};
module.exports.resolveResidencePlace = resolveResidencePlace;

var resolveOccupation = function(key, lang) {
    var occupation = new Occupation();
    return occupation.getValueString(key, lang);
};
module.exports.resolveOccupation = resolveOccupation;

var resolveSmoking = function(key, lang) {
    var smoking = new Smoking();
    return smoking.getValueString(key, lang);
};
module.exports.resolveSmoking = resolveSmoking;

var resolveDrinking = function(key, lang) {
    var drinking = new Drinking();
    return drinking.getValueString(key, lang);
};
module.exports.resolveDrinking = resolveDrinking;

var resolveResidentialStatus = function(key, lang) {
    var residentialStatus = new ResidentialStatus();
    return residentialStatus.getValueString(key, lang);
};
module.exports.resolveResidentialStatus = resolveResidentialStatus;

var resolveLanguage = function(keyArray, lang) {
    var language = new Language();
    return language.getValueString(keyArray, lang);
};
module.exports.resolveLanguage = resolveLanguage;

var resolveReligion = function(keyArray, lang) {
    var religion = new Religion();
    return religion.getValueString(keyArray, lang);
};
module.exports.resolveReligion = resolveReligion;

var resolveRace = function(key, lang) {
    var race = new Race();
    return race.getValueString(key, lang);
};
module.exports.resolveRace = resolveRace;

var resolveIncome = function(key, lang) {
    var income = new Income();
    return income.getValueString(key, lang);
};
module.exports.resolveIncome = resolveIncome;



// Resolve Other
var resolveActivity = function(key, lang) {
    var activity = new Activity();
    return activity.getValueString(key, lang);
};
module.exports.resolveActivity = resolveActivity;