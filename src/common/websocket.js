const io = require('./wx_socketio');

class Socket {
    constructor(host) {
        var self = this;

        this.user_id = getApp().$app.globalData.selfUserInfo.email;
        this.wx_token = getApp().$app.globalData.selfUserInfo.wx_token;
        this.host = host;
        this.connected = false;

        this.socketConnect();
        
        this.attachListeners();
    }

    socketConnect() {
        this.socket = io.connect(this.host, {
            'forceNew': true,
            'query': 'user_id=' + this.user_id + '&wx_token=' + this.wx_token
        });
    }

    sendMessage(recipient_id, message, ackCallback) {
        if (!this.socket.connected) {
            console.log('not connected');
            socketConnect();
            return;
        }
        var payload = {
            "message": message,
            "recipient_id": recipient_id,
            "mode": "PRIVATE_MESSAGE"
        };
        this.socket.emit('direct message', payload, function(data) { // callback param is not supported by current wx_socketio implementation
            if (data.success) {
                console.log('message sent successfully');
            } else {
                console.log('failed to send the message');
            }
        });
    }

    attachListeners() {
        var self = this;

        this.socket.on('connect', function() {
            console.log('websocket connected');
        });

        this.socket.on('disconnect', function() {
            console.log('websocket disconnected');
        });
    }
}

export default Socket;