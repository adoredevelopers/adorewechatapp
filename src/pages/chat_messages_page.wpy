<!-- style -->
<style lang="less">
    @import '../common/stylesheet/color';

    #chat-conversations {
        width: 100%;
        height: 100%;

        .message-row {
            display: flex;
            flex-wrap: nowrap;
            padding: 20rpx;

            &.incoming-message {
                float: left;
                margin-right: 110rpx;
                justify-content: flex-start;

                .message-triangle {
                    border-right: 20rpx solid @ADORE_RED_COLOR_WITH_ALPHA;
                }
            }

            &.outgoing-message {
                float: right;
                margin-left: 110rpx;
                justify-content: flex-end;

                .message-triangle {
                    border-left: 20rpx solid @ADORE_RED_COLOR_WITH_ALPHA;
                }
            }

            image {
                max-width: 80rpx;
                height: 80rpx;
                border-radius: 50%;
            }

            .message-triangle {
                margin-top: 25rpx;
                width: 0;
                height: 0;
                border-top: 12rpx solid transparent;
                border-bottom: 12rpx solid transparent;
            }

            .message-content {
                padding: 10rpx;
                border-radius: 10rpx;
                background: @ADORE_RED_COLOR_WITH_ALPHA;

                text {
                    color: @BLACK_GREY_COLOR;
                }
            }

            .flex-spacer {
                width: 10rpx;
            }
        }

        #bottom-anchor {
            height: 140rpx;
        }
    }

    #chat-input-controls {
        display: flex;
        position: fixed;
        left: 0;
        bottom: 0;
        width: 100%;
        height: 100rpx;
        background-color: @LIGHT_YELLOW_COLOR_WITH_ALPHA;
        // border-top: 1rpx solid @ADORE_RED_COLOR;

        .input-control {
            padding: 10rpx;
            height: 100%;

            image {
                height: 80rpx;
                width: 80rpx;
            }
        }

        #text-input-control {
            flex: 1;
            height: 100%;

            input {
                height: 80rpx;
                color: @BLACK_GREY_COLOR;
                font-size: 28rpx;
                line-height: 32rpx;
                border: 1rpx dotted @ADORE_RED_COLOR;
                padding: 0 10rpx;
                background-color: @PAGE_BACKGROUND_DEFAULT_COLOR;
                box-sizing: border-box;
            }
        }
    }
</style>

<!-- view -->
<template>
    <!-- root needs to be scroll-view in order to take up the whole screen so scroll-top and scroll-into-view will work -->
    <scroll-view id="chat-conversations" scroll-y="true" scroll-into-view="{{ presentationLayerKVs.scrollToElementId }}" bindtap="hideKeyboard()">
        <block wx:for="{{ chatMessages }}" wx:for-index="index" wx:for-item="message" wx:key="*this">
            <block wx:if="{{ message.sender_id !== selfUserInfo.email }}">
                <!-- incoming -->
                <view id="{{ message._id }}" class="message-row incoming-message clear">
                    <image src="{{ userInfo.normalized_display_image_urls[0] }}" bindtap="goToUserDetailsPage()" />
                    <view class="flex-spacer" />
                    <view class="message-triangle"></view>
                    <view class="message-content">
                        <text>{{ message.message }}</text>
                    </view>
                </view>
            </block>
            <block wx:else>
                <!-- outgoing -->
                <view id="{{ message._id }}" class="message-row outgoing-message clear">
                    <view class="message-content">
                        <text>{{ message.message }}</text>
                    </view>
                    <view class="message-triangle"></view>
                    <view class="flex-spacer" />
                    <image src="{{ selfUserInfo.normalized_display_image_urls[0] }}" />
                </view>
            </block>
        </block>
        <view id="bottom-anchor" class="clear" />
    </scroll-view>
    <view id="chat-input-controls">
        <view id="smiley-control" class="input-control" bindtap="showSmiley()">
            <image src="../images/icons/smiley-color-outline.png" />
        </view>
        <view id="text-input-control" class="input-control">
            <input type="text" placeholder="请输入消息" value="{{ inputChatMessage }}" auto-focus="true" bindblur="updateInputChatMessage()" bindconfirm="updateInputChatMessage()" />
        </view>
        <view id="send-message-btn" class="input-control" bindtap="sendMessage()">
            <image src="../images/icons/send-message-color-outline.png" />
        </view>
    </view>
</template>

<!-- script -->
<script>
    import wepy from 'wepy';

    var utils = require('../../src/common/utils');

    export default class ChatMessagePage extends wepy.page {
        config = {
            'navigationBarTitleText': '聊天'
        };

        data = {
            'pageName': null,
            'userInfo': null,
            'selfUserInfo': null,
            'chatMessages': [],
            'inputChatMessage': '',
            'presentationLayerKVs': {
                'scrollToElementId': ''
            },
            'refPageName': null
        };

        methods = {
            hideKeyboard() {
                wx.hideKeyboard();
            },
            showSmiley(data, e) {
                wx.showToast({
                    'title': 'Coming soon!',
                    'icon': 'success'
                });
            },
            updateInputChatMessage(data, e) {
                this.inputChatMessage = e.detail.value;
            },
            sendMessage(data, e) {
                var message = this.inputChatMessage;
                this.$wxapp.$app.socket.sendMessage(this.userInfo.email, message, function(ackData) {
                    console.log(ackData);
                });
                var messageRecord = {
                    'sender_id': this.selfUserInfo.email,
                    'recipient_id': this.userInfo.email,
                    'message': message,
                    'timestamp': Date.now()
                };
                this.chatMessages.push(messageRecord);
                this.updateStoredChatMessages();
                this.inputChatMessage = '';
                this.$apply();
            },
            goToUserDetailsPage(data, e) {
                var self = this;

                var pageName = 'user_details_page';
                if (pageName === self.refPageName) {
                    wx.navigateBack();
                } else {
                    wx.navigateTo({
                        'url': 'user_details_page?ref=chat_message_page&userInfoJSONString=' + JSON.stringify(self.userInfo)
                    });
                }
            }
        };

        onLoad(params) {
            var self = this;

            this.pageName = this.$name;

            
            this.refPageName = params.ref;
            this.userInfo = JSON.parse(params.userInfoJSONString);
            wx.getStorage({
                'key': 'chat_conversations',
                'success': function(result) {
                    var chatConversations = result.data;
                    if (chatConversations[self.userInfo.email] != null) {
                        self.chatMessages = chatConversations[self.userInfo.email].messages;
                    } else {
                        // self.chatMessages = [];
                        var requestData = {
                            'wx_token': self.selfUserInfo.wx_token,
                            'conversation_user_id': self.userInfo.email
                        };
                        utils.sendAPIRequest({
                            'apiName': 'get_user_conversation',
                            'queryString': utils.buildRequestQueryString(requestData),
                            'requestContentType': 'application/json',
                            'requestData': requestData
                        }, function(res) {
                            self.chatMessages = res.data.data;
                            self.$apply();
                            if (self.chatMessages.length > 0) {
                                self.updateStoredChatMessages();
                            }
                        }, function(err) {
                            self.chatMessages = [];
                            self.$apply();
                        }, null);
                    }
                },
                'fail': function(err) {
                    console.log(err);
                    wx.setStorage({
                        'key': 'chat_conversations',
                        'data': {}
                    });
                    self.chatMessages = [];
                }
            });

            wx.setNavigationBarTitle({
                'title': this.userInfo.normalized_display_name
            });

            this.$wxapp.$app.getSelfUserInfo(function(userInfo) {
                self.selfUserInfo = userInfo;
            });

            this.$wxapp.$app.socket.socket.on('private message', function(data) {
                delete data['mode'];
                delete data['socket_message_id'];
                self.chatMessages.push(data);
                self.updateStoredChatMessages();
            });
        };

        onReady() {
            var self = this;
            
            this.scrollToBottom();
        };

        updateStoredChatMessages() {
            var self = this;

            wx.getStorage({
                'key': 'chat_conversations',
                'success': function(result) {
                    var chatConversations = result.data;
                    for (let idx in self.chatMessages) {
                        self.chatMessages[idx].date_time_string = utils.getReadableDateTimeString(self.chatMessages[idx].timestamp, false);
                    }
                    if (chatConversations[self.userInfo.email] == null) {
                        chatConversations[self.userInfo.email] = {
                            'user_info': self.userInfo,
                            'remove_btn_class': 'hidden',
                            'new_message_class': 'hidden',
                            'messages': self.chatMessages
                        };
                    } else {
                        chatConversations[self.userInfo.email].messages = self.chatMessages;
                    }
                    wx.setStorage({
                        'key': 'chat_conversations',
                        'data': chatConversations,
                        'success': function() {
                            self.scrollToBottom();
                        }
                    });
                }
            });
        };

        scrollToBottom() {
            // 1. this is equal to $(document).ready(); scroll-top, scroll-into-view value has to be changed here to make it work
            // 2. assigning to '' empty first, not sure why it has this behaviour
            this.presentationLayerKVs.scrollToElementId = ''
            this.$apply();
            this.presentationLayerKVs.scrollToElementId = 'bottom-anchor';
            this.$apply();
        };
    }
</script>