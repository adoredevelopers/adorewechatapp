<!-- style -->
<style lang="less">
    @import '../common/stylesheet/color';

    #user-list-view {
        background-color: @PAGE_BACKGROUND_DEFAULT_COLOR;
        
        .user-list-item-view {
            display: flex;
            flex-direction: column;
            width: 100%;
            margin-bottom: 5px;
            background-color: @WHITE_COLOR;
            overflow: hidden;
            cursor: pointer;

            .user-list-item-top-view {
                width: 100%;
                height: 180rpx;

                .user-image {
                    float: left;
                    padding: 10rpx;
                    width: 160rpx;
                    height: 160rpx;
                    border-radius: 10rpx;

                    image {
                        width: 100%;
                        height: 100%;
                        border-radius: 10rpx;
                    }
                }

                .user-info-split {
                    display: flex;
                    flex: 1;
                    padding: 10rpx 10rpx 10rpx 0;
                    height: 160rpx;

                    .user-info-left {
                        display: flex;
                        flex: 1;
                        flex-direction: column;
                    }

                    .user-info-right {
                        display: flex;
                        flex-direction: column;
                        padding-left: 5rpx;
                        text-align: right;
                        color: @DARK_GREY_COLOR;

                        text {
                            font-size: 22rpx;
                        }
                    }

                    .user-info-row {
                        flex: 1;
                        height: 100%;
                        margin: 10rpx;

                        .info-text-view {
                            line-height: 32rpx;

                            &.info-main {
                                color: @BLACK_GREY_COLOR;

                                text {
                                    font-size: 32rpx;
                                }
                            }

                            &.info-secondary {
                                color: @DARK_GREY_COLOR;

                                text {
                                    font-size: 24rpx;
                                }
                            }
                        }
                    }
                }
            }

            .user-list-item-bottom-view {
                max-height: 116rpx;

                .info-text-view {
                    padding: 10rpx;
                    overflow: hidden;
                    color: @DARK_GREY_COLOR;
                    font-size: 24rpx;
                    line-height: 32rpx;
                    max-height: 90rpx;
                }
            }
        }
    }
</style>

<!-- view -->
<template>
    <view id="body">
        <view id="user-list-view" scroll-y="true">
            <block wx:for="{{ nearbyUsers }}" wx:for-index="index" wx:for-item="user" wx:key="email">
                <view class="user-list-item-view" bindtap="goToUserDetailsPage({{ user }})">
                    <view class="user-list-item-top-view">
                        <view class="user-image">
                            <image mode="aspectFill" src="{{ user.normalized_display_image_urls[0] }}"></image>
                        </view>
                        <view class="user-info-split">
                            <view class="user-info-left">
                                <view class="user-info-row">
                                    <view class="info-text-view info-main">
                                        <text>{{ user.normalized_display_name }}</text>
                                        <text>{{ ', ' + user.basicInfo.gender.text }}</text>
                                        <text>{{ user.basicInfo.age.text != null ? ', ' + user.basicInfo.age.text + '岁' : '' }}</text>
                                    </view>
                                </view>
                                <view class="user-info-row">
                                    <view class="info-text-view info-secondary">
                                        <text>{{ user.basicInfo.zodiac.text }}</text>
                                    </view>
                                </view>
                                <view class="user-info-row">
                                    <view class="info-text-view info-secondary">
                                        <text>{{ user.detailedInfo.residence_place.text != null ? user.detailedInfo.residence_place.text : '' }}</text>
                                    </view>
                                </view>
                            </view>
                            <view class="user-info-right">
                                <view class="user-info-view">
                                    <view class="info-text-view info-secondary">
                                        <text>{{ user.normalized_distance }}公里</text>
                                    </view>
                                </view>
                                <view class="user-info-view">
                                    <view class="info-text-view info-secondary">
                                        <block wx:if="{{ user.minutes_since_last_activity < 15 }}">
                                            <text>online now</text>
                                        </block>
                                        <block wx:elif="{{ user.minutes_since_last_activity >= 15 && user.minutes_since_last_activity < 60 }}">
                                            <text>15分钟前</text>
                                        </block>
                                        <block wx:elif="{{ user.minutes_since_last_activity >= 60 && user.minutes_since_last_activity < 120 }}">
                                            <text>1小时前</text>
                                        </block>
                                        <block wx:elif="{{ user.minutes_since_last_activity >= 120 && user.minutes_since_last_activity < 300 }}">
                                            <text>2小时前</text>
                                        </block>
                                        <block wx:elif="{{ user.minutes_since_last_activity >= 300 && user.minutes_since_last_activity < 720 }}">
                                            <text>5小时前</text>
                                        </block>
                                        <block wx:elif="{{ user.minutes_since_last_activity >= 720 && user.minutes_since_last_activity < 1440 }}">
                                            <text>半天前</text>
                                        </block>
                                        <block wx:elif="{{ user.minutes_since_last_activity >= 1440 && user.minutes_since_last_activity < 10080 }}">
                                            <text>1天前</text>
                                        </block>
                                        <block wx:elif="{{ user.minutes_since_last_activity >= 10080 && user.minutes_since_last_activity < 43200 }}">
                                            <text>1周前</text>
                                        </block>
                                        <block wx:else>
                                            <text>1个月前</text>
                                        </block>
                                    </view>
                                </view>
                            </view>
                        </view>
                    </view>
                    <view wx:if="{{ user.moreInfo.about_me.text != null }}" class="user-list-item-bottom-view">
                        <view class="info-text-view">
                            {{ user.moreInfo.about_me.text }}
                        </view>
                    </view>
                </view>
            </block>
        </view>
    </view>
</template>

<!-- script -->
<script>
    import wepy from 'wepy';

    var constants = require('../../src/common/constants');
    var utils = require('../../src/common/utils');

    export default class UserListPage extends wepy.page {
        config = {
            'navigationBarTitleText': '附近用户',
            'enablePullDownRefresh': true
        };

        data = {
            'pageName': null,
            'selfUserInfo': null,
            'filterParameters': {
                'mode': 'normal',
                'batch_size': constants.constants.common.nearby_user_fetch_constants.batch_size,
                'longitude': -79.556561,
                'latitude': 43.796788,
                'max_distance': 100000,
                'min_age': 18,
                'max_age': 40,
                'gender': '', // defaulted to the opposite sex
                'wx_token': ''
            },
            'allowedPagingCount': 5,
            'currentPagingCount': 0,
            'noMorePages': false,
            'nearbyUsers': []
        };

        methods = {
            goToUserDetailsPage(data, e) {
                wx.navigateTo({
                    'url': 'user_details_page?userInfoJSONString=' + JSON.stringify(data)
                });
            }
        };

        onLoad(params) {
            var self = this;

            this.pageName = this.$name;

            var mode = 'normal';
            this.$wxapp.$app.getSelfUserInfo(function(userInfo) {
                self.selfUserInfo = userInfo;
                if (constants.constants.common.account_types.premium.indexOf(self.selfUserInfo.account_type) != -1) {
                    self.allowedPagingCount = constants.constants.common.nearby_user_fetch_constants.allowed_paging_count_premium;
                } else {
                    self.allowedPagingCount = constants.constants.common.nearby_user_fetch_constants.allowed_paging_count_normal
                }
                self.fetchNearbyUsers(mode, function(success) {
                    self.refreshCompleted(mode, success);
                });
            });
        };

        onPullDownRefresh() {
            var self = this;

            var mode = 'normal';
            this.fetchNearbyUsers(mode, function(success) {
                self.refreshCompleted(mode, success);
            });
        };

        onReachBottom() {
            var self = this;

            var mode = 'paging';
            if (this.currentPagingCount < this.allowedPagingCount) {
                if (this.noMorePages) {
                    wx.showToast({
                        'title': '没有了',
                        'icon': 'success'
                    });
                } else {
                    this.fetchNearbyUsers(mode, function(success) {
                        self.refreshCompleted(mode, success);
                    });
                }
            } else {
                wx.showToast({
                    'title': '已到达上限',
                    'icon': 'success'
                });
            }
        };

        refreshCompleted(mode, success) {
            var self = this;

            if (mode === 'paging') {
                wx.hideNavigationBarLoading();
            } else {
                wx.stopPullDownRefresh({
                    'complete': function (res) {
                        wx.hideToast();
                    }
                });
            }

            if (success != null && !success) {
                wx.hideToast();
                wx.showModal({
                    'title': '刷新失败',
                    'content': '兜酱在刷新途中被怪蜀黍领走了。。。',
                    'showCancel': false
                });
            }
        };

        fetchNearbyUsers(mode, cb) {
            var self = this;

            if (mode === 'paging') {
                wx.showNavigationBarLoading();
            } else {
                wx.showToast({
                    'title': '载入中...',
                    'icon': 'loading',
                    'mask': true,
                    'duration': 15000
                });
            }
            wx.getStorage({
                'key': 'wx_token',
                'success': function(result) {
                    console.log(result);
                    self.filterParameters.wx_token = result.data;
                    wepy.getLocation({
                        success (res) {
                            console.log('location: ' + res);
                            sendGetNearbyUserRequest(result.data, res, mode, cb);
                        },
                        fail (err) {
                            console.log(err);
                            sendGetNearbyUserRequest(result.data, null, mode, cb);
                        }
                    });
                }
            });

            function sendGetNearbyUserRequest(wx_token, coords, mode, cb) {
                self.filterParameters.mode = mode;
                if (coords != null) {
                    self.filterParameters.longitude = coords.longitude;
                    self.filterParameters.latitude = coords.latitude;
                }
                self.$apply();

                utils.sendAPIRequest({
                    'apiName': 'get_nearby_users',
                    'queryString': utils.buildRequestQueryString(self.filterParameters),
                    'requestContentType': 'application/json',
                    'requestData': self.filterParameters
                }, function(res) {
                    var success = res.data.success;
                    var nearbyUsers = res.data.data;
                    if (success) {
                        console.log(nearbyUsers);
                        if (nearbyUsers.length < self.filterParameters.batch_size) {
                            self.noMorePages = true;
                        }
                        nearbyUsers = nearbyUsers.map(utils.normalizeUserInfo);
                        if (self.filterParameters.mode === 'paging') {
                            self.nearbyUsers = self.nearbyUsers.concat(nearbyUsers);
                            self.currentPagingCount = self.currentPagingCount + 1;
                        } else {
                            self.nearbyUsers = nearbyUsers;
                            self.currentPagingCount = 0;
                            self.noMorePages = false;
                        }
                        self.$apply();
                    }
                    cb && cb(true);
                }, function(err) {
                    console.log(err);
                    cb && cb(false);
                });
            }
        };
    }
</script>